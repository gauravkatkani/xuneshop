<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';

$route['frontend'] 				= 'FrontendController/index';
$route['customer'] 				= 'CustomerController/index';


$route['admin'] 	 			= 'AdminController/index';
$route['admin/login'] 	 		= 'AdminController/login';
$route['404_override'] 			= '';
$route['translate_uri_dashes'] 	= FALSE;


//$route['admin/logout'] 					= 'Ajadmin/AdminController/logout';
$route['admin/dashboard'] 				= 'AdminController/dashboard';
$route['admin/dashboard/home'] 			= 'AdminController/home';
$route['admin/getCountRowsOnDashboard'] = 'AdminController/getCountRowsOnDashboard';

//Dashboard views with module and page
$route['admin/dashboard/(:any)/(:any)'] = 'AdminController/getViews/$1/$2';

//product routes
$route['admin/dashboard/productStore'] 	= 'Admin/ProductController/productStore';
$route['admin/dashboard/productUpdate'] = 'Admin/ProductController/productUpdate';
$route['admin/dashboard/productDelete'] = 'Admin/ProductController/productDelete';
$route['admin/dashboard/getProductDataById'] = 'Admin/ProductController/getProductDataById';
$route['admin/dashboard/getProductListWithDetail'] = 'Admin/ProductController/getProductListWithDetail';
$route['admin/dashboard/synchWithShopify'] = 'Admin/ProductController/synchWithShopify';

//customer routes
$route['admin/dashboard/customerStore'] 	= 'Admin/CustomerController/customerStore';
$route['admin/dashboard/customerUpdate']	= 'Admin/CustomerController/customerUpdate';
$route['admin/dashboard/customerDelete'] 	= 'Admin/CustomerController/customerDelete';
$route['admin/dashboard/getCustomerDataById'] 		= 'Admin/CustomerController/getcustomerDataById';
$route['admin/dashboard/getCustomerListWithDetail'] = 'Admin/CustomerController/getCustomerListWithDetail';
$route['admin/dashboard/customerSynchWithShopify'] = 'Admin/CustomerController/customerSynchWithShopify';

//order routes
$route['admin/dashboard/orderStore'] 	= 'Admin/OrderController/orderStore';
$route['admin/dashboard/orderUpdate']	= 'Admin/OrderController/orderUpdate';
$route['admin/dashboard/orderDelete'] 	= 'Admin/OrderController/orderDelete';
$route['admin/dashboard/getOrderDataById'] 		= 'Admin/OrderController/getOrderDataById';
$route['admin/dashboard/getOrderListWithDetail'] = 'Admin/OrderController/getOrderListWithDetail';
$route['admin/dashboard/getOrderFullfilmentDetail'] = 'Admin/OrderController/getOrderFullfilmentDetail';
$route['admin/dashboard/orderSynchWithShopify'] = 'Admin/OrderController/orderSynchWithShopify';

//order routes
$route['admin/dashboard/credentialStore'] 	= 'Admin/CredentialController/credentialStore';
$route['admin/dashboard/credentialUpdate']	= 'Admin/CredentialController/credentialUpdate';
$route['admin/dashboard/credentialDelete'] 	= 'Admin/CredentialController/credentialDelete';
$route['admin/dashboard/getCredentialDataById'] 		= 'Admin/CredentialController/getCredentialDataById';
$route['admin/dashboard/getCredentialListWithDetail'] = 'Admin/CredentialController/getCredentialListWithDetail';

