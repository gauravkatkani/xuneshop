<?php 
class AdminModel extends CI_Model {
	
   	public function __construct()
	{		
		parent::__construct();
		
		$this->load->database();
		$this->load->library('form_validation');
		
	}

	public function getRowCount($table){
		return $this->db->count_all($table);
	}


	public function getProductStatusCount($productstage)
    { 
    	$this->db->where('published',$productstage);
		$this->db->from('product');
		$count = $this->db->count_all_results();
         if(isset($count))
         {
         	return $count;
         }else
         {
         	return 0;
         }
    }

    public function login(){
		if(!empty($this->input->post('password')) && !empty($this->input->post('email'))){
			$where = array('email' =>$this->input->post('email') ,'password' => md5($this->input->post('password')));
	        $query = $this->db->select('*')->from('admin')->where($where)->get();
	        
	        if($query->num_rows() > 0)
	        {
	        	$result 	= 	$query->row();	        	
	        	$authToken  = 	md5(date('Y-m-d H:i:s'));	        	
	    		$data 		=	array(
				                    'email' 		 	=> 	$result->email,
				                    'first_name'	 	=>  $result->first_name,
				                    'last_name'	     	=>  !empty($result->last_name) ? $result->last_name :'',
				                    'image' 		 	=> 	$result->image,
				                    'user_type' 	 	=> 	'admin',
				                    'loggedin' 		 	=> 	TRUE,
				                    'admin_full_name'	=>  $result->first_name." ".@$result->last_name,
				                    'Admin_Auth_Token'	=>	$authToken
				                );
	        	
				$this->session->set_userdata($data);
				
				return  json_encode(array('status'=>1,'message'=>'Login Successfully','data'=>$data));
	        }
	        else{
	        	return  json_encode(array('status'=>0,'message'=>'Invalid Credential','data'=>FALSE));
	        }
		}else{
			return  json_encode(array('status'=>0,'message'=>'Email & Password Required','data'=>FALSE));
		}
	}

	public function getProductListWithDetail(){
 		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$query = $this->db->select('*')->from('product')->order_by('product.id','DESC')->get();			
				//echo $this->db->last_query();die;
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()));
				}else{
					return  json_encode(array('status'=>1,'message'=>'No record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
 	}


 	public function addShopifyProduct($userdata){

 		//$url = "https://{{apikey}}:{{password}}@{{shopname}}.myshopify.com/admin/products.json";

 		$url = "https://65686211550bc7e8ab2f17225b4fcbe9:6b258e7b4a33a4db6b58d49e40fc5a67@diksha63.myshopify.com/admin/products.json";

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $userdata);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec ($curl);
		return $response; 
		curl_close ($curl);
	}

	public function getShopifyProduct(){

 		//$url = "https://{{apikey}}:{{password}}@{{shopname}}.myshopify.com/admin/products.json";

 		$url = "https://65686211550bc7e8ab2f17225b4fcbe9:6b258e7b4a33a4db6b58d49e40fc5a67@diksha63.myshopify.com/admin/products.json";

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $userdata);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec ($curl);
		print_r($response);
		die();
		return $response; 
		curl_close ($curl);
	}

	public function getOrderShopifyProduct(){

 		//$url = "https://{{apikey}}:{{password}}@{{shopname}}.myshopify.com/admin/products.json";

 		$url = "https://65686211550bc7e8ab2f17225b4fcbe9:6b258e7b4a33a4db6b58d49e40fc5a67@diksha63.myshopify.com/admin/orders.json";

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 1);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $userdata);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec ($curl);
		print_r($response);
		die();
		return $response; 
		curl_close ($curl);
	}

 	public function productStore(){

		if(!empty($_REQUEST['Admin_Auth_Token']) && $_REQUEST['Admin_Auth_Token'] == $this->session->Admin_Auth_Token )
		{
			if(!empty($_REQUEST['user_type']) && $_REQUEST['user_type'] == 'admin' )
			{

				$product_details =	array(	
										'title'=>$_REQUEST['product_name'],
					      				'product_type'=>$_REQUEST['product_type'],
					      				'vendor'=>$_REQUEST['vendor'],
					      				'created_at'=>date('Y-m-d H:i:s'),
					      				'tags'=>$_REQUEST['tags'],
					      				'published'=>$_REQUEST['publish'],
				      					'shopify'=>$_REQUEST['shopify'],
				      					'price'=>$_REQUEST['price'],
					      				'variants'=>$_REQUEST['variant']
				      				);			 

				if(!empty($_FILES)){
					$uploadData                     = array();
					$config['upload_path']          = './assets/img/product_img/';
			        $config['allowed_types']        = 'gif|jpg|png|svg';
			        $config['max_size']             = 5120;
			        $config['max_width']            = 1900;
			        $config['max_height']           = 1900;
			        $config['encrypt_name']         = true;

			        $this->load->library('upload', $config);

			        if (!$this->upload->do_upload('file'))
			        {		           	
			            return  json_encode(array('status'=>0,'message'=>$this->upload->display_errors()));
			        }
			        else
			        {
			            $uploadData = $this->upload->data();
			            $flag  = true;
			            $product_details['image'] = !empty($uploadData)?$uploadData['file_name']:'';
			            http://localhost/xuneshop/assets/img/product_img/6bc5c5096c807fbff2ea04b10a0ff5e1.png
			        }
				}

		       	$query = $this->db->insert('product',$product_details);
		       	
				if($query){

					$product_id = $this->db->insert_id();

					$selectquery = $this->db->select('title,body_html,vendor,product_type,tags,price,image')->from('product')->where('id',$product_id)->get();

					$getProductDetails=$selectquery->row();

					$shopifyResponse = $this->addShopifyProduct(json_encode( array('product'=>$getProductDetails)));

					//$getShopifyProduct = $this->getShopifyProduct();

					print_r($shopifyResponse);
					die();

					$getOrderProductDetails = $this->getOrderShopifyProduct();

                    
					return  json_encode(array('status'=>1,'message'=>'Product Insert Successfully','data'=>$getProductDetails));
				}else{
					return  json_encode(array('status'=>0,'message'=>'product Insert Failed'));
				}		
				
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function productDelete(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				if(!empty($this->input->post('product_id'))){

					$this->db-> where('id',$this->input->post('product_id'));

    				if($this->db-> delete('product')){

						return  json_encode(array('status'=>1,'message'=>'Delete Product Details'));
					}
					else{
						return  json_encode(array('status'=>0,'message'=>'Failed'));
					}

				}else{
					return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
				}		 		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function getProductDataById(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin')
			{

				$query = $this->db->select('*')->from('product')->where('product.id', $this->input->post('product_id'))->get();				
		       	  
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()[0]));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'No Record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function productUpdate(){

		if(!empty($_REQUEST['Admin_Auth_Token']) && $_REQUEST['Admin_Auth_Token'] == $this->session->Admin_Auth_Token )
		{
			if(!empty($_REQUEST['user_type']) && $_REQUEST['user_type'] == 'admin' )
			{

				$flag 			= false;
		        
				$product_details =	array(	
									'title'=>$_REQUEST['product_name'],
				      				'product_type'=>$_REQUEST['product_type'],
				      				'vendor'=>$_REQUEST['vendor'],
				      				'created_at'=>date('Y-m-d H:i:s'),
				      				'tags'=>$_REQUEST['tags'],
				      				'published'=>$_REQUEST['publish'],
				      				'shopify'=>$_REQUEST['shopify'],
				      				'price'=>$_REQUEST['price'],
				      				'variants'=>$_REQUEST['variant']
			      				);  

				if(!empty($_FILES)){

					$uploadData                     = array();
					$config['upload_path']          = './assets/img/product_img/';
			        $config['allowed_types']        = 'gif|jpg|png|svg';
			        $config['max_size']             = 5120;
			        $config['max_width']            = 1900;
			        $config['max_height']           = 1900;
			        $config['encrypt_name']         = true;

			        $this->load->library('upload', $config);

			        if (!$this->upload->do_upload('file'))
			        {		           	
			            return  json_encode(array('status'=>0,'message'=>$this->upload->display_errors()));
			        }
			        else
			        {
			            $uploadData = $this->upload->data();
			            $flag  = true;
			            $product_details['image'] = !empty($uploadData)?$uploadData['file_name']:'';
			        }

				}
				
				$this->db->set($product_details)->where('id', $_REQUEST['product_id']);	
				// $this->db->update('product');
				// echo $this->db->last_query();	
				// die();		
       	  
				if($this->db->update('product')){

					if($flag && !empty($_REQUEST['old_img'])){
						unlink("./assets/img/product_img/".$_REQUEST['old_img']);
					}
                    
					return  json_encode(array('status'=>1,'message'=>'Update Client Successfully'));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'Update Client Failed'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	function getToken($length)
	{
	    $token = "";
	    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	    $max = strlen($codeAlphabet); // edited

	    for ($i=0; $i < $length; $i++) {
	        $token .= $codeAlphabet[random_int(0, $max-1)];
	    }
	    return $token;
	}

	function getOtp($length)
	{
	    $token = "";
	    $codeAlphabet = "0123456789";
	    $max = strlen($codeAlphabet); // edited

	    for ($i=0; $i < $length; $i++) {
	        $token .= $codeAlphabet[random_int(0, $max-1)];
	    }
	    return $token;
	}

	public function checkIfInvoiceExist(){
		$query = $this->db->select('id')->from('client')->where(array('invoice'=>$this->input->post('invoice'),'voucher_status'=>1))->get();
		return ($query->num_rows() > 0) ? true :false;
	}

	public function checkIfEmailExist(){
		$query = $this->db->select('id')->from('client')->where(array('email'=>$this->input->post('email'),'voucher_status'=>1))->get();
		return ($query->num_rows() > 0) ? true :false;
	}

	public function checkIfNumberExist(){
		$query = $this->db->select('id')->from('client')->where(array('number'=>$this->input->post('number'),'voucher_status'=>1))->get();
		return ($query->num_rows() > 0) ? true :false;
	}

	
	public function submitClientDetails(){

		if(!empty($this->input->post()))
		{
			if($this->checkIfEmailExist()){
				return json_encode(array('status'=>0,'message'=>'Email Already Exist'));
			}
			if($this->checkIfNumberExist()){
				return json_encode(array('status'=>0,'message'=>'Number Already Exist'));
			}						
			//$voucher = $this->getToken(7);
			$otp = $this->getOtp(4);

			$client_details =	array(	
				'name'		=> $this->input->post("name"),
				'number'	=> $this->input->post("number"),
				'email'		=> $this->input->post("email"),
				'comment'	=> $this->input->post("comment"),
				'interest'	=> json_encode($this->input->post("interest")),
				//'voucher'	=> $voucher,
				'voucher_otp' => $otp
			);	

			$query = $this->db->insert('client',$client_details);
			$client_id = $this->db->insert_id();

			if($query){

				//send mail 
		        $Subject = "Email Verification from Sarvodaya.in";     
		      	$Content = "Dear ".$this->input->post("name").",";   
		        $Content.= "<br>Greetings from Sarvodaya Traders!";
		        $Content.= "<br>Thanks for your submission at sarvodaya.in, to activate your voucher, please enter the OTP given below:";
		     	$Content.="<br><br>";
		     	$Content.=$otp;		     	
		     	$Content.="<br><br>"; 
		        $Content .= "Or use below link to complete the process.";
		        $Content .= "<br> <a href='http://www.ZZZZZZZZZZ.com'>http://www.ZZZZZZZZZZ.com</a>";
		        $Content .= "<br><br>Thanks & regards";
		        $Content .= "<br>Customer Support - Sarvodaya Traders";
		        $Content .= "<br>Email: customer@sarvodaya.in";
		        $Content .= "<br>Tel: +91-11-45513228 / 29";
		        $header = "Content-type: text/html";
		        	                     
				mail($this->input->post("email"), $Subject, $Content,$header);

				//send sms

				$message = "Use OTP ".$otp." to activate your Voucher.";

				$url = 'http://msg.icloudsms.com/rest/services/sendSMS/sendGroupSms?AUTH_KEY=e6cc3f154ae6a96cdc5a9ea5567e079&message='.urlencode($message).'&senderId=DEMOOS&routeId=8&mobileNos='.$this->input->post("number").'&smsContentType=english';

				$result = file_get_contents($url);
				$result = json_decode( $result );

				if(!empty($result->response)){
					return json_encode(array('status'=>1,'message'=>'Voucher create successfully','data'=>$client_id));
				}else{
					return json_encode(array('status'=>0,'message'=>'Message sending failed'));
				}
				return json_encode(array('status'=>1,'message'=>'Voucher create successfully'));

			}else{
				return json_encode(array('status'=>0,'message'=>'Missing Parameter'));
			}	
		}else{
			return json_encode(array('status'=>0,'message'=>'Connection Missing'));
		}
	}

	public function verifyClientOtp(){
		if(!empty($this->input->post()))
		{		

			if(!empty($this->input->post("clientId")) && !empty($this->input->post("otp"))){

				$where =array(	
					'id'		=> $this->input->post("clientId"),
					'voucher_otp'	=> $this->input->post("otp"),
				);	

				$query = $this->db->select('*')->from('client')->where($where)->get();			
				//echo $this->db->last_query();die;
				if($query->num_rows() > 0){
					$results = $query->row();
					//send mail 
					$voucher = $this->getToken(7);

					$this->db->set('voucher',$voucher)->where('id',$this->input->post('clientId'));	

					if($this->db->update('client')){
					
				        $Subject = "Voucher Code: ".$results->voucher." from Sarvodaya.in";     
				      	$Content = "Dear ".$results->name.",";   
				        $Content.= "<br>Congratulations!";
				        $Content.= "<br>We are happy to share your Voucher below.";
				     	$Content.="<br><br>";
				     	$Content.=$results->voucher;		     	
				     	$Content.="<br><br>"; 
				        $Content .= "Please feel free to contact us for any further information.";
				        $Content .= "<br><br>Thanks & regards";
				        $Content .= "<br>Customer Support - Sarvodaya Traders";
				        $Content .= "<br>Email: customer@sarvodaya.in";
				        $Content .= "<br>Tel: +91-11-45513228 / 29";
				        $header = "Content-type: text/html";
				        	                     
						mail($this->input->post("email"), $Subject, $Content,$header);

						//send sms

						$message = "Your Voucher No. ".$results->voucher." is active. Please carry Voucher No. to redeem it at our Showroom – Sarvodaya Traders, 66, Furniture Block, Kirti Nagar, New Delhi";

						$url = 'http://msg.icloudsms.com/rest/services/sendSMS/sendGroupSms?AUTH_KEY=e6cc3f154ae6a96cdc5a9ea5567e079&message='.urlencode($message).'&senderId=DEMOOS&routeId=8&mobileNos='.$results->number.'&smsContentType=english';

						$result = file_get_contents($url);
						$result = json_decode( $result );

						if(!empty($result->response)){
							return json_encode(array('status'=>1,'message'=>'Voucher create successfully'));
						}else{
							return json_encode(array('status'=>0,'message'=>'Message sending failed'));
						}
					}else{
						return json_encode(array('status'=>0,'message'=>'Network Error'));
					}
				}else{
					return  json_encode(array('status'=>0,'message'=>'Invalid Otp'));
				}

			}else{
				return json_encode(array('status'=>0,'message'=>'Please Enter Otp'));	
			}			
		}else{
			return json_encode(array('status'=>0,'message'=>'Connection Missing'));
		}
	}

	public function getClientListWithDetail(){
 		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$query = $this->db->select('*')->from('client')->where('voucher !=','')->order_by('client.id','DESC')->get();			
				// echo $this->db->last_query();die;
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()));
				}else{
					return  json_encode(array('status'=>0,'message'=>'No record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
 	}

	public function redeemVoucher(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				if(!empty($this->input->post('client_id'))){

					$client_id = $this->input->post('client_id');

					$query = $this->db->select('*')->from('client')->where('id',$this->input->post('client_id'))->get();

					$data = $query->row();	

					if($data->voucher_status == 0){

						//$voucher = $this->getToken(7);
						$otp = $this->getOtp(4);
						$voucher = $data->voucher;

						$this->db->set('redeem_otp',$otp)->where('id',$this->input->post('client_id'));	

						if($this->db->update('client')){

							$query = $this->db->select('*')->from('client')->where('id',$client_id)->get();

							$data = $query->row();

							$Subject = "OTP to Redeem Voucher from Sarvodaya.in";     
					      	$Content = "Dear ".$data->name.",";   
					        $Content.= "<br>Greetings from Sarvodaya Traders!";
					        $Content.= "<br>Please share the OTP given below at the Counter of Sarvodaya Traders Showroom to redeem the Voucher Offer:";
					     	$Content.="<br><br>";
					     	$Content.=$otp;		     	
					     	$Content.="<br><br>";    
					        $Content .= "Thanks & regards";
					        $Content .= "<br>Customer Support - Sarvodaya Traders";
					        $Content .= "<br>Email: customer@sarvodaya.in";
					        $Content .= "<br>Tel: +91-11-45513228 / 29";
					        $header = "Content-type: text/html";

							mail($data->email, $Subject, $Content, $header);

							//send sms

							$message = "Please share this OTP ".$otp." with Cash Counter to redeem your Voucher Offer - Sarvodaya Traders";

							$url = 'http://msg.icloudsms.com/rest/services/sendSMS/sendGroupSms?AUTH_KEY=e6cc3f154ae6a96cdc5a9ea5567e079&message='.urlencode($message).'&senderId=DEMOOS&routeId=8&mobileNos='.$data->number.'&smsContentType=english';

							$result = file_get_contents($url);
							$result = json_decode( $result );

							if(!empty($result->response)){

								return json_encode(array('status'=>1,'message'=>'Otp Generate Successfully','data'=>$data));
							}else{
								return json_encode(array('status'=>0,'message'=>'Network Failure','data'=>$data));
							}
						}
						else{
							return json_encode(array('status'=>0,'message'=>'Failed'));
						}

					}else{
						return json_encode(array('status'=>0,'message'=>'Voucher Already Redeem'));
					}				

				}else{
					return json_encode(array('status'=>0,'message'=>'Missing Parameters'));
				}	
			}else{
				return json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}


	public function otpVerify(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				if(!empty($this->input->post('client_id'))){

					if($this->checkIfInvoiceExist()){
						return json_encode(array('status'=>0,'message'=>'Invoice Already Used','data'=>[]));
					}
					$where =array(	
							'id'=>$this->input->post('client_id'),
							'redeem_otp'=>$this->input->post('otp'),
						);

					$query = $this->db->select('*')->from('client')->where($where)->get();

					if($query->num_rows() > 0){

						$data = $query->row();	

						$client_details =	array(	
							'voucher_status'=>1,
							'invoice'=>$this->input->post("invoice"),
						);	

						$this->db->set($client_details)->where('id',$this->input->post('client_id'));	

						if($this->db->update('client')){

							$Subject = "Voucher Redeemed from Sarvodaya.in";     
					      	$Content = "Dear ".$data->name.",";   
					        $Content.= "<br>Thanks for your purchase at Sarvodaya Traders!";
					        $Content.= "<br>We would like to inform you that your Voucher Code: ".$data->voucher." had been redeemed.";
					     	$Content.="<br><br>";
					     	$Content.="Please feel free to contact us for any further information.";		     	
					     	$Content.="<br><br>";    
					        $Content .= "Thanks & regards";
					        $Content .= "<br>Customer Support - Sarvodaya Traders";
					        $Content .= "<br>Email: customer@sarvodaya.in";
					        $Content .= "<br>Tel: +91-11-45513228 / 29";
					        $header = "Content-type: text/html";

							mail($data->email, $Subject, $Content,$header);

							//send sms

							$message = "Thanks for your purchase at Sarvodaya Traders. Your Voucher Offer had been redeemed - Sarvodaya Traders";

							$url = 'http://msg.icloudsms.com/rest/services/sendSMS/sendGroupSms?AUTH_KEY=e6cc3f154ae6a96cdc5a9ea5567e079&message='.urlencode($message).'&senderId=DEMOOS&routeId=8&mobileNos='.$data->number.'&smsContentType=english';

							$result = file_get_contents($url);
							$result = json_decode( $result );

							if(!empty($result->response)){

								return json_encode(array('status'=>1,'message'=>'Voucher Redeemed Successfully','data'=>$data));
							}else{
								return json_encode(array('status'=>0,'message'=>'Network Failure','data'=>$data));
							}
							return json_encode(array('status'=>1,'message'=>'Otp Match Successfully'));
						}
						else{
							return json_encode(array('status'=>0,'message'=>'Failed'));
						}

					}else{
						return json_encode(array('status'=>0,'message'=>'Otp Not Match'));
					}

				}else{
					return json_encode(array('status'=>0,'message'=>'Missing Parameters'));
				}	
			}else{
				return json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

}
	

?> 