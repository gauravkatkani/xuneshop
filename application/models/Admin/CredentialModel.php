<?php 
class CredentialModel extends CI_Model {
	
   	public function __construct()
	{		
		parent::__construct();
		
		$this->load->database();
		$this->load->library('form_validation');
		
	}

	public function getCredentialListWithDetail(){
 		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$query = $this->db->select('*')->from('credentials')->order_by('credentials.id','DESC')->get();			
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()));
				}else{
					return  json_encode(array('status'=>1,'message'=>'No record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
 	}

 	public function credentialStore(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{

				$credentialDetails 	=	array(

					'vendor'			=>$this->input->post('vendor'),
      				'api_key'			=>$this->input->post('apikey'),
      				'api_secret'		=>$this->input->post('secret'),
      				'password'			=>$this->input->post('password'),
      				'shop_url'			=>$this->input->post('url')
  				);			 

		       	$query = $this->db->insert('credentials',$credentialDetails);
		       	
				if($query){

					$credential_id = $this->db->insert_id();
					
					return  json_encode(array('status'=>1,'message'=>'Credential Insert Successfully'));
                    
				}else{
					return  json_encode(array('status'=>0,'message'=>'Credential Insert Failed'));
				}		
				
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function credentialDelete(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				if(!empty($this->input->post('credential_id'))){

					$this->db-> where('id',$this->input->post('credential_id'));

    				if($this->db->delete('credentials')){

						return  json_encode(array('status'=>1,'message'=>'Delete Credential Details'));
					}
					else{
						return  json_encode(array('status'=>0,'message'=>'Failed'));
					}

				}else{
					return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
				}		 		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function getCredentialDataById(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin')
			{

				$query = $this->db->select('*')->from('credentials')->where('credentials.id', $this->input->post('credential_id'))->get();				

				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->row()));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'No Record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function credentialUpdate(){
	
		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
		        
				$credentialsDetails 	=	array(
					'vendor'			=>$this->input->post('vendor'),
      				'api_key'			=>$this->input->post('apikey'),
      				'api_secret'		=>$this->input->post('secret'),
      				'password'			=>$this->input->post('password'),
      				'shop_url'			=>$this->input->post('url'),
      				'created_at'		=>date('Y-m-d H:i:s')
  				);
				
				$this->db->set($credentialsDetails)->where('id', $this->input->post('credential_id'));	
				
				if($this->db->update('credentials')){

					return  json_encode(array('status'=>1,'message'=>'Update Credentials Successfully'));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'Update Credentials Failed'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	
}
	

?> 