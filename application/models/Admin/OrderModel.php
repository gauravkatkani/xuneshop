<?php 
class OrderModel extends CI_Model {
	
   	public function __construct()
	{		
		parent::__construct();
		
		$this->load->database();
		$this->load->library('form_validation');
		
	}

	public function getOrderListWithDetail(){
 		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$response = json_decode($this->getShopifyOrderDetail());
				// $query = $this->db->select('*')->from('order')->order_by('order.id','DESC')->get();			
				// if($query->num_rows() > 0){
				// 	return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()));
				// }else{
				// 	return  json_encode(array('status'=>1,'message'=>'No record Found'));
				// }
				return  json_encode(array('status'=>1,'message'=>'success','data'=>$response));		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
 	}

 	public function getOrderFullfilmentDetail(){
 		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				if(!empty($this->input->post('order_id')))
				{
					$order_id = $this->input->post('order_id');
					$response = json_decode($this->getShopifyOrderFullfilmentDetail($order_id));

					return  json_encode(array('status'=>1,'message'=>'success','data'=>$response));		
				}else{
					return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
 	}

	public function deleteShopifyOrderDetails($id){
 		
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/customers/#".$id.".json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		//curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($customerDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
		
		return $response; 
	}
	
 	public function getShopifyCustomerById($id){

 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/customers/".$id.".json"; 		
		return file_get_contents($url); 
		
	}

	public function getShopifyOrderDetail(){

 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/orders.json"; 		

 		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($customerDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
		return $response;
		//return file_get_contents($url); 
		
	}

	public function getShopifyOrderFullfilmentDetail($id){

 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/orders/#".$id."/fulfillments.json"; 		

 		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($customerDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
		return $response;
		//return file_get_contents($url); 
		
	}

	public function orderDelete(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				if(!empty($this->input->post('customer_id'))){

					$this->db-> where('id',$this->input->post('customer_id'));

    				if($this->db-> delete('customer')){

						return  json_encode(array('status'=>1,'message'=>'Delete Product Details'));
					}
					else{
						return  json_encode(array('status'=>0,'message'=>'Failed'));
					}

				}else{
					return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
				}		 		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function getOrderDataById(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin')
			{

				$query = $this->db->select('*')->from('customer')->where('customer.id', $this->input->post('customer_id'))->get();				
		       	  
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()[0]));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'No Record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function orderSynchWithShopify(){
 		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$response = json_decode($this->getShopifyOrderDetail());

				$counter  = 0;
				foreach ($response->orders as $key => $value) {
					//print_r($response->customers);
					//die();
					$query = $this->db->select('*')->from('orders')->where('orders.shopify_orders_id', $value->id)->get();

					$orderDetails 	=	array(
						'shopify_orders_id'   	=>$value->id,
						'order_number'			=>$value->order_number,
	      				'customer_name'			=>$value->billing_address->name,
	      				'customer_email'		=>$value->email,
	      				'status'				=>$value->financial_status,
	      				'price'					=>$value->total_price,
	      				'city'					=>$value->billing_address->city,
	  					'state'					=>$value->billing_address->province,
	      				'country'				=>$value->billing_address->country,
	      				'payment'				=>$value->gateway,
	  				);	
					if($query->num_rows() > 0){
						$this->db->set($orderDetails)->where('id', $query->row()->id);		
						$this->db->update('orders');
					}else{
						$query = $this->db->insert('orders',$orderDetails);
					}
				}

				return  json_encode(array('status'=>1,'message'=>'success','data'=>$response));		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
 	}

	
}
	

?> 