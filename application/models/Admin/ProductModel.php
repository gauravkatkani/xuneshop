<?php 
class ProductModel extends CI_Model {
	
   	public function __construct()
	{		
		parent::__construct();		
		$this->load->database();
		$this->load->library('form_validation');
		
	}

	public function getProductStatusCount($productstage)
    {    	
    	return $this->db->where('published',$productstage)->from('product')->count_all_results();
    }

	public function getProductListWithDetail(){
 		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$query = $this->db->select('*')->from('product')->order_by('product.id','DESC')->get();			
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()));
				}else{
					return  json_encode(array('status'=>1,'message'=>'No record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
 	}

 	public function addProductToShopify($productDetails){
 	
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/products.json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($productDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
	
		return $response;
	}

	public function getShopifyProductDetailList(){
 		
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/products.json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($productDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
	
		return $response;
	}

	public function getShopifyProductById($id){

 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/products/".$id.".json"; 		
		return file_get_contents($url); 
		
	}

	public function updateShopifyProductImage($image,$id){

 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/products/#".$id."/images.json";
 	
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($image));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
		return $response; 
	}


	public function updateShopifyProductDetails($productDetails,$id){
 		
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/products/#".$id.".json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($productDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
		
		return $response; 
	}

	public function deleteShopifyProductDetails($id){
 		
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/products/#".$id.".json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		//curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($customerDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
		
		return $response; 
	}

 	public function productStore(){

		if(!empty($_REQUEST['Admin_Auth_Token']) && $_REQUEST['Admin_Auth_Token'] == $this->session->Admin_Auth_Token )
		{
			if(!empty($_REQUEST['user_type']) && $_REQUEST['user_type'] == 'admin' )
			{

				$productDetails 	=	array(

					'title'			=>$_REQUEST['product_name'],
      				'product_type'	=>$_REQUEST['product_type'],
      				'vendor'		=>$_REQUEST['vendor'],
      				'created_at'	=>date('Y-m-d H:i:s'),
      				'tags'			=>$_REQUEST['tags'],
      				'published'		=>$_REQUEST['publish'],
  					'shopify'		=>$_REQUEST['shopify'],
  					'price'			=>$_REQUEST['price'],
      				'variants'		=>$_REQUEST['variant']
  				);			 

				if(!empty($_FILES)){
					$uploadData                     = array();
					$config['upload_path']          = './assets/img/product_img/';
			        $config['allowed_types']        = 'gif|jpg|png|svg';
			        $config['max_size']             = 5120;
			        $config['max_width']            = 1900;
			        $config['max_height']           = 1900;
			        $config['encrypt_name']         = true;

			        $this->load->library('upload', $config);

			        if (!$this->upload->do_upload('file'))
			        {		           	
			            return  json_encode(array('status'=>0,'message'=>$this->upload->display_errors()));
			        }
			        else
			        {
			            $uploadData = $this->upload->data();
			            $flag  		= true;
			            $productDetails['image'] = !empty($uploadData)?$uploadData['file_name']:'';
			        }
				}

		       	$query = $this->db->insert('product',$productDetails);
		       	
				if($query){

					$product_id = $this->db->insert_id();
					if($_REQUEST['shopify']==1){
						unset($productDetails['created_at']);
						unset($productDetails['shopify']);
						unset($productDetails['images']);
						$productDetails['published'] == 1 ? true :false;
						$productDetails['body_html'] = "Good snowboard";

						$productDetails['images'] = [array("images"=>base_url('assets/img/product_img/').$uploadData['file_name'])];

						
						$data = array("product"=>$productDetails);

						$response = json_decode($this->addProductToShopify($data));

						// $response1 = json_decode($this->updateShopifyProductImage($image,$response->product->id));

						$this->db->set('shopify_id',$response->product->id)->where('id',$product_id);	
						$this->db->update('product');
					}

					return  json_encode(array('status'=>1,'message'=>'Product Insert Successfully'));
                    
				}else{
					return  json_encode(array('status'=>0,'message'=>'product Insert Failed'));
				}		
				
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function productDelete(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				if(!empty($this->input->post('product_id'))){

					$this->db-> where('id',$this->input->post('product_id'));

    				if($this->db-> delete('product')){

						return  json_encode(array('status'=>1,'message'=>'Delete Product Details'));
					}
					else{
						return  json_encode(array('status'=>0,'message'=>'Failed'));
					}

				}else{
					return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
				}		 		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function synchWithShopify(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$response = json_decode($this->getShopifyProductDetailList());
				$counter  = 0;
				foreach ($response->products as $key => $value) {
					// print_r($response->products);
					// die();
					$query = $this->db->select('*')->from('product')->where('product.shopify_id', $value->id)->get();
			
					$productDetails 	=	array(
						'shopify_id'    =>$value->id,
						'title'			=>$value->title,
	      				'product_type'	=>$value->product_type,
	      				'vendor'		=>$value->vendor,
	      				'created_at'	=>date('Y-m-d H:i:s'),
	      				'tags'			=>$value->tags,
	      				'published'		=>!empty($value->published_at) ? 1:0,
	  					'shopify'		=>1,
	  					'price'			=>$value->variants[0]->price
	  				);

	  				// print_r($productDetails);
	  				// die();
					if($query->num_rows() > 0){
						$this->db->set($product_details)->where('id', $query->row()->id);		
						$this->db->update('product');
					}else{
						$query = $this->db->insert('product',$productDetails);
					}
					
				}
				
				return  json_encode(array('status'=>1,'message'=>'Synch With Shopify Successfully','data'=>$response));		 	
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function getProductDataById(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin')
			{

				$query = $this->db->select('*')->from('product')->where('product.id', $this->input->post('product_id'))->get();				
		       	  
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()[0]));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'No Record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function productUpdate(){

		if(!empty($_REQUEST['Admin_Auth_Token']) && $_REQUEST['Admin_Auth_Token'] == $this->session->Admin_Auth_Token )
		{
			if(!empty($_REQUEST['user_type']) && $_REQUEST['user_type'] == 'admin' )
			{

				$flag 			= false;
		        
				$product_details =	array(	
									'title'			=>$_REQUEST['product_name'],
				      				'product_type'	=>$_REQUEST['product_type'],
				      				'vendor'		=>$_REQUEST['vendor'],
				      				'created_at'	=>date('Y-m-d H:i:s'),
				      				'tags'			=>$_REQUEST['tags'],
				      				'published'		=>$_REQUEST['publish'],
				      				'shopify' 		=>$_REQUEST['shopify'],
				      				'price'			=>$_REQUEST['price'],
				      				'variants'=>$_REQUEST['variant']
			      				);  

				if(!empty($_FILES)){

					$uploadData                     = array();
					$config['upload_path']          = './assets/img/product_img/';
			        $config['allowed_types']        = 'gif|jpg|png|svg';
			        $config['max_size']             = 5120;
			        $config['max_width']            = 1900;
			        $config['max_height']           = 1900;
			        $config['encrypt_name']         = true;

			        $this->load->library('upload', $config);

			        if (!$this->upload->do_upload('file'))
			        {		           	
			            return  json_encode(array('status'=>0,'message'=>$this->upload->display_errors()));
			        }
			        else
			        {
			            $uploadData = $this->upload->data();
			            $flag  = true;
			            $product_details['image'] = !empty($uploadData)?$uploadData['file_name']:'';
			        }

				}
				
				$this->db->set($product_details)->where('id', $_REQUEST['product_id']);	
				
				if($this->db->update('product')){

					if($flag && !empty($_REQUEST['old_img'])){
						unlink("./assets/img/product_img/".$_REQUEST['old_img']);
					}
                    
					return  json_encode(array('status'=>1,'message'=>'Update Client Successfully'));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'Update Client Failed'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	
}
	

?> 