<?php 
class CustomerModel extends CI_Model {
	
   	public function __construct()
	{		
		parent::__construct();
		
		$this->load->database();
		$this->load->library('form_validation');
		
	}

	public function getCustomerListWithDetail(){
 		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$query = $this->db->select('*')->from('customer')->order_by('customer.id','DESC')->get();			
				//echo $this->db->last_query();die;
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()));
				}else{
					return  json_encode(array('status'=>1,'message'=>'No record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
 	}

	public function addCustomerToShopify($customerDetails){
 		
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/customers.json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($customerDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
		
		return $response; 
	}

	public function updateShopifyCustomerDetails($customerDetails,$id){
 		
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/customers/#".$id.".json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($customerDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}

		print_r($response);	
		curl_close ($curl);
			die();	
		return $response; 
	}

	public function deleteShopifyCustomerDetails($id){
 		
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/customers/#".$id.".json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}
		curl_close ($curl);
		
		return $response; 
	}
	
 	public function getShopifyCustomerById($id){

 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/customers/".$id.".json"; 		
		return file_get_contents($url); 
		
	}

	public function getShopifyCustomerDetailList(){
 		
 		$url = "https://".$this->config->item('SHOPIFY_API_KEY').":".$this->config->item('SHOPIFY_PASSWORD')."@".$this->config->item('SHOPIFY_SHOP')."/admin/customers.json";
 		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_VERBOSE,0);
		curl_setopt($curl, CURLOPT_HEADER,false);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($productDetails));
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
		    return 'Error:' . curl_error($curl);
		}	
		curl_close ($curl);
	
		return $response;
	}

 	public function customerStore(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{

				$customerDetails 	=	array(

					'first_name'		=>$this->input->post('first_name'),
      				'last_name'			=>$this->input->post('last_name'),
      				'email'				=>$this->input->post('email'),
      				'send_email_invite'	=>$this->input->post('send_email_invite'),
      				'phone'				=>$this->input->post('phone'),
      				'address'			=>$this->input->post('address'),
  					'city'				=>$this->input->post('city'),
      				'country'			=>$this->input->post('country')
  				);			 

		       	$query = $this->db->insert('customer',$customerDetails);
		       	
				if($query){

					$customer_id = $this->db->insert_id();
						unset($customerDetails['address']);
						unset($customerDetails['city']);
						unset($customerDetails['country']);

						$customerAddress = array(

							'first_name'		=>$this->input->post('first_name'),
		      				'last_name'			=>$this->input->post('last_name'),
		      				'phone'				=>$this->input->post('phone'),
		      				'address'			=>$this->input->post('address'),
		  					'city'				=>$this->input->post('city'),
		      				'country'			=>$this->input->post('country')
		  				);
						$customerDetails['addresses'] = array("addresses"=>$customerAddress);
						
						$data = array("customer"=>$customerDetails);

						$response = json_decode($this->addCustomerToShopify($data));
						
						$this->db->set('shopify_customer_id',$response->customer->id)->where('id',$customer_id);	
						$this->db->update('customer');

					return  json_encode(array('status'=>1,'message'=>'Product Insert Successfully'));
                    
				}else{
					return  json_encode(array('status'=>0,'message'=>'product Insert Failed'));
				}		
				
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function customerDelete(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				if(!empty($this->input->post('customer_id'))){

					$this->db-> where('id',$this->input->post('customer_id'));

    				if($this->db->delete('customer')){

    		// 			$query = $this->db->select('*')->from('customer')->where('customer.id', $this->input->post('customer_id'))->get();

						// $data = $query->row();	

						// if($query->num_rows() > 0){

						// 	$response = json_decode($this->deleteShopifyCustomerDetails($data->shopify_customer_id));

						// 	print_r($response);
						// 	die();

						// 	return  json_encode(array('status'=>1,'message'=>'Delete Product Details'));
						// }
						// else{
						// 	return  json_encode(array('status'=>0,'message'=>'Failed'));
						// }

						return  json_encode(array('status'=>1,'message'=>'Delete Product Details'));
					}
					else{
						return  json_encode(array('status'=>0,'message'=>'Failed'));
					}

				}else{
					return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
				}		 		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function customerSynchWithShopify(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{
				$response = json_decode($this->getShopifyCustomerDetailList());
				$counter  = 0;
				foreach ($response->customers as $key => $value) {
					//print_r($response->customers);
					//die();
					$query = $this->db->select('*')->from('customer')->where('customer.shopify_customer_id', $value->id)->get();

					$customerDetails 	=	array(
						'shopify_customer_id'   =>$value->id,
						'first_name'			=>$value->first_name,
	      				'last_name'				=>$value->last_name,
	      				'email'					=>$value->email,
	      				//'send_email_invite'		=>$value->,
	      				'phone'					=>$value->phone,
	      				//'address'				=>$value->default_address->address,
	  					'city'					=>$value->default_address->city,
	      				'country'				=>$value->default_address->country
	  				);	
					if($query->num_rows() > 0){
						$this->db->set($customerDetails)->where('id', $query->row()->id);		
						$this->db->update('customer');
					}else{
						$query = $this->db->insert('customer',$customerDetails);
					}
				}
				return  json_encode(array('status'=>1,'message'=>'Synch With Shopify Successfully','data'=>$response));		 	
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function getCustomerDataById(){

		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin')
			{

				$query = $this->db->select('*')->from('customer')->where('customer.id', $this->input->post('customer_id'))->get();				
		       	  
				if($query->num_rows() > 0){
					return  json_encode(array('status'=>1,'message'=>'success','data'=>$query->result()[0]));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'No Record Found'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	public function customerUpdate(){
	
		if(!empty($this->input->post('Admin_Auth_Token')) && $this->input->post('Admin_Auth_Token') == $this->session->Admin_Auth_Token )
		{
			if(!empty($this->input->post('user_type')) && $this->input->post('user_type') == 'admin' )
			{

				$flag 			= false;
		        
				$customerDetails 	=	array(

					'first_name'		=>$this->input->post('first_name'),
      				'last_name'			=>$this->input->post('last_name'),
      				'email'				=>$this->input->post('email'),
      				'send_email_invite'	=>$this->input->post('send_email_invite'),
      				'phone'				=>$this->input->post('phone'),
      				'address'			=>$this->input->post('address'),
  					'city'				=>$this->input->post('city'),
      				'country'			=>$this->input->post('country')
  				);
				
				$this->db->set($customerDetails)->where('id', $this->input->post('customer_id'));	
				
				if($this->db->update('customer')){

					// $query = $this->db->select('*')->from('customer')->where('customer.id', $this->input->post('customer_id'))->get();

					// $data = $query->row();	

					// if($query->num_rows() > 0){

					// 	unset($data->address);
					// 	unset($data->city);
					// 	unset($data->country);
					// 	print_r($data);
					// 	echo $data->first_name;

					// 	$customerAddress = array(

					// 		'first_name'		=>$data->first_name,
		   //    				'last_name'			=>$data->last_name,
		   //    				'phone'				=>$data->phone,
		   //    				'address'			=>$data->address,
		  	// 				'city'				=>$data->city,
		   //    				'country'			=>$data->country
		  	// 			);
					// 	$data1['addresses'] = array("addresses"=>$customerAddress);
						
					// 	$data1 = array("customer"=>$data);

					// 	$response = json_decode($this->updateShopifyCustomerDetails($data1,$data->shopify_customer_id));

					// 	return  json_encode(array('status'=>1,'message'=>'Update Client Successfully'));
					// }
					// else{
					// 	return  json_encode(array('status'=>0,'message'=>'Failed'));
					// }		

					return  json_encode(array('status'=>1,'message'=>'Update Client Successfully'));
				}
				else{
					return  json_encode(array('status'=>0,'message'=>'Update Client Failed'));
				}		
			}else{
				return  json_encode(array('status'=>0,'message'=>'Missing Parameters'));
			}
		}
		else{
			return  json_encode(array('status'=>0,'message'=>'Not Authorize'));
		}
	}

	
}
	

?> 