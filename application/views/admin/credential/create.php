
<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">Vendor
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select type="text" ng-model="vendor"  class="form-control col-md-7 col-xs-12" ng-options="o.id as o.value for o in vendorList" ></select>
          
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_type">Api Key
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="last-name" ng-model="apikey" name="last-name" class="form-control col-md-7 col-xs-12"  maxlength="100">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vendor">Secret Key
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="last-name" ng-model="secret" class="form-control col-md-7 col-xs-12"  maxlength="100">
        </div>
      </div>
      <div class="form-group">
        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Password </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input ng-model="password" id="email" class="form-control col-md-7 col-xs-12" type="text" >
        </div>
      </div>
      <div class="form-group">
        <label for="variantd" class="control-label col-md-3 col-sm-3 col-xs-12">Shop Url </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control col-md-7 col-xs-12" ng-model="url" >
        </div>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <button class="btn btn-primary" ui-sref="admin.dashboard.credential.show" >Cancel</button>
          <button ng-click="submit()" class="btn btn-success">Submit</button>
        </div>
      </div>

    </form>

  </div>

  

    
</div>
<!-- /page content -->



