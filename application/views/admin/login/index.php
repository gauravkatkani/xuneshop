<div class="login_wrapper">
  <div class="animate form login_form">
    <section class="login_content">
      <form method="post"  >
        <h1>Login Form</h1>
        <div>
          <input type="email" class="form-control" placeholder="Email "  ng-model="email" />
        </div>
        <div>
          <input type="password" class="form-control" placeholder="Password"  ng-model="password" />
        </div>
        <div>
          <button class="btn btn-default submit"  ng-click="submit()">Log in</button>
          <a class="reset_pass" href="#">Lost your password?</a>
        </div>

        <div class="clearfix"></div>
      </form>
    </section>
  </div>
</div>