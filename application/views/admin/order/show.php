
<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Order Management</h2>
          <ul class="nav navbar-right panel_toolbox">
            <!-- <li><span><a href="javascript:void(0)"  class="btn btn-success pull-right"> <i class="fa fa-envelope-o"></i> Send Mail</a ></span>
            </li>
            <li><span><a href="javascript:void(0)"  class="btn btn-success pull-right"> <i class="fa fa-paper-plane"></i> Send SMS</a ></span>
            </li> -->
            <li><span><a ng-click="orderSynchWithShopify()" class="btn btn-success pull-right"> <i class="fa fa-paper-plane"></i> Synchronize with Shopify</a ></span>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="right-side">  
              <div id="grid1" ui-grid="gridOptions" ui-grid-edit ui-grid-pagination ui-grid-selection ui-grid-pinning class="grid"></div>
            </div>                
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->


  
