
<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">First Name
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" ng-model="first_name"  class="form-control col-md-7 col-xs-12"  maxlength="100">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_type">Last Name
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="last-name" ng-model="last_name" name="last-name" class="form-control col-md-7 col-xs-12"  maxlength="100">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vendor">Mobile
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="last-name" ng-model="phone" class="form-control col-md-7 col-xs-12"  maxlength="100">
        </div>
      </div>
      <div class="form-group">
        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input ng-model="email" id="email" class="form-control col-md-7 col-xs-12" type="text" >
        </div>
      </div>
      <div class="form-group">
        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Send Email Invite </label>
        <div class="col-md-6 col-sm-6 col-xs-12">    
          <input type="checkbox" class="js-switch" ng-model="send_email_invite" checked />      
        </div>
      </div>
      <div class="form-group">
        <label for="variantd" class="control-label col-md-3 col-sm-3 col-xs-12">Address </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control col-md-7 col-xs-12" ng-model="address" >
        </div>
      </div>
      <div class="form-group">
        <label for="variantd" class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control col-md-7 col-xs-12" ng-model="city" >
        </div>
      </div>
      <div class="form-group">
        <label for="variantd" class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control col-md-7 col-xs-12" ng-model="country" >
        </div>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <button class="btn btn-primary" ui-sref="admin.dashboard.customer.show" >Cancel</button>
          <button ng-click="submit()" class="btn btn-success">Submit</button>
        </div>
      </div>

    </form>

  </div>

  

    
</div>
<!-- /page content -->



