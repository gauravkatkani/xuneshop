<!DOCTYPE html>
<html lang="en" ng-app="App">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">     
    <title>Xune! | </title>     
    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->
     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Bootstrap -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/animate.css/animate.min.css" rel="stylesheet">
    <!-- tables  -->
    <link rel="stylesheet"; href="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.css">
    <!-- toaster -->
    <link rel="stylesheet" href="https://unpkg.com/angular-toastr/dist/angular-toastr.css" />

    <!-- angualr material library -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.12/angular-material.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/4.8.0/ui-grid.min.css">
    <!-- Custom Theme Style -->
    <link href="<?= base_url() ?>vendor/gentelella/build/css/custom.min.css" rel="stylesheet">

    <style type="text/css">
        .right_col{
            min-height: 650px;
        }
         /*ui grid css*/
        .myGrid {
            width: 100%;
            height: 250px;
        }
        .full-custom-select .multiselect-parent.btn-group.dropdown-multiselect {
            width: 100%;
        }
        .admin-detail-form-sec {
            width: 80%;
            margin: 0 auto;
        }
        .has-error {
            border:1px solid red !important;
        }

        .has-success {
            border:1px solid green !important;
        }
        .capitalize {
            text-transform: capitalize;
        }
        .intl-tel-input {
            position: relative;
            display: inline-block;
            width: 100%;
        }

        .custom-autocomplete .md-whiteframe-1dp, .custom-autocomplete .md-whiteframe-z1 {
            box-shadow: none;
            background: #fff;
            border: 1px solid #ccc;
            background: #f9f9f9;
            border-radius: 2px;
            color: #444;
        }
        /*showing all content of rows */
        .custom-btn{
          padding: 0px 5px;
        }

       /* modal css*/
        p.close-modal {
            position: absolute;
            right: 10px;
            top: 5px;
            z-index: 999;
        }
        md-dialog#otpDialog {
            width: 500px;
            height: 210px;
        }
        .modal-footer {
            margin-top: 50px;
            text-align: right;
            border-top: 1px solid #fff; */
        }
        .otp-box{
            margin-top: 20px;
        }
        div#clientDetail {
            padding-top: 60px;
        }



    </style>
  </head>

  <body>
    <div class="container body">
      <div class="main_container">         
        <ui-view></ui-view>
      </div>
    </div>
    <script type="text/javascript">
      var BaseUrl = "<?= base_url() ?>index.php/";
    </script>
    <!-- jQuery -->
    <script src="<?= base_url() ?>vendor/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url() ?>vendor/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?= base_url() ?>vendor/gentelella/vendors/iCheck/icheck.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?= base_url() ?>vendor/gentelella/build/js/custom.min.js"></script>
    
    <!-- user management page script -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js" ></script> 
    <!-- toaster -->
    <script src="https://unpkg.com/angular-toastr/dist/angular-toastr.tpls.js"></script>
    <!-- ui-router -->
    <script src="https://angular-ui.github.io/ui-router/release/angular-ui-router.js" ></script>
    <!-- ui-grid pre-requites -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/4.8.0/ui-grid.min.js"></script>
    <!-- ng-tables -->
    <script src="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.js"></script>
    <script src="<?= base_url() ?>assets/js/ng-file-upload-shim.min.js"></script>
    <script src="<?= base_url() ?>assets/js/ng-file-upload.min.js"></script> 
    <!-- Angular Material requires Angular.js Libraries -->
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular-animate.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular-aria.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular-messages.min.js"></script>
      <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/t-114/svg-assets-cache.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.12/angular-material.min.js"></script>
      <!-- Angular Material Library -->
       <!-- app js -->
    <script src="<?= base_url() ?>assets/js/adminApp.js"></script>
    
  </body>
</html>
