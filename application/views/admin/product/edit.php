
<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">Product Name
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" ng-model="product_name"  class="form-control col-md-7 col-xs-12"  maxlength="100">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_type">Product Type
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="last-name" ng-model="product_type" name="last-name" class="form-control col-md-7 col-xs-12"  maxlength="100">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vendor">Vendor
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="last-name" ng-model="vendor" class="form-control col-md-7 col-xs-12"  maxlength="100">
        </div>
      </div>
      <div class="form-group">
        <label for="tags" class="control-label col-md-3 col-sm-3 col-xs-12">Tags </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input ng-model="tags" id="tags" class="form-control col-md-7 col-xs-12" type="text" >
        </div>
      </div>
      <div class="form-group">
        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Published </label>
        <div class="col-md-6 col-sm-6 col-xs-12">    
          <input type="checkbox" class="js-switch" ng-model="publish" checked />      
        </div>
      </div>
      <div class="form-group">
        <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Shopify </label>
        <div class="col-md-6 col-sm-6 col-xs-12">    
          <input type="checkbox" class="js-switch" ng-model="shopify" checked />      
        </div>
      </div>
      <div class="form-group">
        <label for="variantd" class="control-label col-md-3 col-sm-3 col-xs-12">Variants </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control col-md-7 col-xs-12" ng-model="variant" >
        </div>
      </div>
      <div class="form-group">
        <label for="variantd" class="control-label col-md-3 col-sm-3 col-xs-12">Price</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" class="form-control col-md-7 col-xs-12" ng-model="price" >
        </div>
      </div>
      <div class="form-group">
        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Product Image</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          
          <!-- Single Image with validations -->
          <div class="form-control" ngf-select ng-model="file" name="file" ngf-pattern="'image/*'" ngf-accept="'image/*'" ngf-max-size="20MB" ngf-min-height="100">Upload Image</div>
          <!--Single Image with validations-->
          <br>
          <img ngf-thumbnail="file || '/thumb.jpg'" height="100px">
          <br>
        </div>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <button class="btn btn-primary" ui-sref="admin.dashboard.product.show" >Cancel</button>
          <button ng-click="submit()" class="btn btn-success">Submit</button>
        </div>
      </div>

    </form>

  </div>

  

    
</div>
<!-- /page content -->



