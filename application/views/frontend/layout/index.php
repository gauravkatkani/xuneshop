<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">     
    <title>Xune! | </title>     
    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->
     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Bootstrap -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?= base_url() ?>vendor/gentelella/vendors/animate.css/animate.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?= base_url() ?>vendor/gentelella/build/css/custom.min.css" rel="stylesheet">

    <style type="text/css">
        .right_col{
            min-height: 650px;
        }
         /*ui grid css*/
        .myGrid {
            width: 100%;
            height: 250px;
        }
        .full-custom-select .multiselect-parent.btn-group.dropdown-multiselect {
            width: 100%;
        }
        .admin-detail-form-sec {
            width: 80%;
            margin: 0 auto;
        }
        .has-error {
            border:1px solid red !important;
        }

        .has-success {
            border:1px solid green !important;
        }
        .capitalize {
            text-transform: capitalize;
        }
        .intl-tel-input {
            position: relative;
            display: inline-block;
            width: 100%;
        }

        .custom-autocomplete .md-whiteframe-1dp, .custom-autocomplete .md-whiteframe-z1 {
            box-shadow: none;
            background: #fff;
            border: 1px solid #ccc;
            background: #f9f9f9;
            border-radius: 2px;
            color: #444;
        }
        /*showing all content of rows */
        div#clientDetail {
            padding-top: 60px;
        }
        .hideImg{
          display: none;
        }
        a#hideImg img{
          width: 100%;
        }
        .img-div{
          width: 100%;
          height: 600px;
        }
        .mobileImg{
          display: none;
        }
        @media only screen and (max-width: 767px) {
          .mobileImg{
            display: block;
            width: 100%;
            height: 100%;
          }
          .desktopImg{
            display: none;
          }
        }
    </style>
  </head>

  <body>
    <div class="container body">
      <div class="main_container">
        <div class="img-div before-register">
          <a id="hideImg">
            <img src="<?= base_url() ?>assets/img/PC-Before-Registration.jpg" class="desktopImg"> 
            <img src="<?= base_url() ?>assets/img/Mobile-Before-Registration.jpg" class="mobileImg"> 
          </a> 
        </div>  
              
        <!-- page content -->
        <div class="right_col hideImg" role="main" id="clientDetail">
          <div class="row">
            <form id="clientForm" class="form-horizontal form-label-left">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_name">Name
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="name" class="form-control col-md-7 col-xs-12"  maxlength="100" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="product_type">Contact Number
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="number"  name="number" class="form-control col-md-7 col-xs-12"  maxlength="100" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="vendor">Email Id
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="email" name="email" class="form-control col-md-7 col-xs-12"  maxlength="100">
                </div>
              </div>
              <div class="form-group">
                <label for="tags" class="control-label col-md-3 col-sm-3 col-xs-12">Interest </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <label>
                  <input type="checkbox" class="js-switch" value="Curtain Fabrics" name="interest[]" />
                  Curtain Fabrics
                  </label>
                  <br>
                  <label>
                  <input type="checkbox" class="js-switch" value="Cushion Covers" name="interest[]" />
                  Cushion Covers
                  </label>
                   <br>
                  <label>
                  <input type="checkbox" class="js-switch" value="Upholstery Fabrics" name="interest[]" />
                  Upholstery Fabrics
                  </label>
                   <br>
                  <label>
                  <input type="checkbox" class="js-switch" value="Wallpapers" name="interest[]" />  
                  Wallpapers
                  </label>
                  <br>
                  <label>
                  <input type="checkbox" class="js-switch" value="Carpets & Rugs" name="interest[]" />  
                    Carpets & Rugs
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Comment Box </label>
                <div class="col-md-6 col-sm-6 col-xs-12">    
                  <textarea id="comment" class="form-control col-md-7 col-xs-12"  maxlength="400">                    
                  </textarea>                   
                </div>
              </div>
              <div class="form-group">
                <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Terms & condation </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <br>    
                  <input type="checkbox" class="js-switch" ng-model="shopify" checked /> 
                  By providing your contact details, you agree to be contacted by Sarvodaya Traders using these details     
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="javascript:void(0)" id="submit" class="btn btn-success">Submit</a>
                </div>
              </div>
            </form>
          </div>   
        </div>
        <!-- /page content -->

        <div class="img-div after-register hideImg">
          <a id="hideImg">
            <img src="<?= base_url() ?>assets/img/PC-After-Registration.jpg" class="desktopImg"> 
            <img src="<?= base_url() ?>assets/img/Mobile-After-Registration.jpg" class="mobileImg"> 
          </a> 
        </div>  
      </div>
    </div>
    <script type="text/javascript">
      var BaseUrl = "<?= base_url() ?>index.php/";
    </script>
    <!-- jQuery -->
    <script src="<?= base_url() ?>vendor/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url() ?>vendor/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?= base_url() ?>vendor/gentelella/vendors/iCheck/icheck.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?= base_url() ?>vendor/gentelella/build/js/custom.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
      var baseurl="<?php echo base_url()?>index.php/";
        function validateEmail(email){         
          if(email == '' || email == null || email == undefined){
            return {status:true,error:'Email Required'};
          }else if(email != '' && email != null && email != undefined){
            //var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
            var regex =  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;           

            if((regex.test(email))){
              return {status:false};
            }else{
              return {status:true,error:'Invalid Email'}; 
            }
          }    
        };
        function validateFirstName(first_name) {
          if(first_name == '' || first_name == null || first_name == undefined){
            return {status:true,error:'Enter your name'};
          }else if(first_name.length < 2 && (/^[a-zA-Z ]+$/.test(first_name))){
            return {status:true,error:'Minimum 2 characters'};     
          }else if(first_name != '' && first_name != null && first_name != undefined){
            var regex = /^[a-zA-Z ]+$/;            
            if(!(regex.test(first_name))){
              return {status:true,error:'Please Enter valid name'};
            }else{
              return {status:false};
            }
          }
        };

      $(document).on('click','#submit',function(){
        var name    = $('#name').val();
        var email   = $('#email').val();
        var comment = $('#comment').val();
        var number  = $('#number').val();
        var interest= [];
        var i       = 0 ;
        var clientId = '';

        if(validateFirstName(name).status){
          swal("Error!", validateFirstName(name).error, "error");
        }else if(number.length < 10){
          swal("Error!", "Please provide your valid number", "error");
        }else if(validateEmail(email).status){
          swal("Error!", validateEmail(email).error, "error");
        }else{
          //put all code here
            $('input[type=checkbox]:checked').each(function () {
               interest[i] = $(this).val();
               i++;
            });


            $.ajax({
              type: "POST",
              url: baseurl+'frontend/submitClientDetails',
              data: {name:name,email:email,number:number,comment:comment,interest:JSON.stringify(interest)},
              cache: false,
              dataType: "json",
              success: function(response){
                if(response.status){
                  clientId = response.data;
                  swal("Please enter otp :", {
                    content: "input",
                  })
                  .then((value) => {
                    $.ajax({
                      type: "POST",
                      url: baseurl+'frontend/verifyClientOtp',
                      data: {clientId:clientId,otp:value},
                      cache: false,
                      dataType: "json",
                      success: function(responses){
                        if(responses.status){
                          $('#clientForm')[0].reset();
                          swal("Good job!", "Voucher Created Successfully!", "success");
                          //$(".after-register").addClass("hideImg"); 
                          $(".after-register").removeClass("hideImg"); 
                        }else{
                          swal("Error!",responses.message,"error");
                        }
                      }
                    });
                  });
                }else{
                    swal("Error!",response.message,"error");
                }
              }
            });
        }
          
      })

      $(document).on('click','#hideImg',function(){
        $(".img-div").addClass("hideImg"); 
        $("#clientDetail").removeClass("hideImg"); 
      })

      
    </script>
    
    
  </body>
</html>
