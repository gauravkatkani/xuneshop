<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class CustomerController extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        $_POST  = json_decode(file_get_contents('php://input'), true);  
        $this->load->helper(array('form', 'url'));
        $this->load->model('Admin/CustomerModel', 'Customer');
	}   

   
    public function customerStore(){
        echo $this->Customer->customerStore(); 
    }
    public function customerUpdate(){
        echo $this->Customer->customerUpdate(); 
    }
    public function customerDelete(){
        echo $this->Customer->customerDelete(); 
    }
    public function getCustomerDataById(){
        echo $this->Customer->getCustomerDataById();    
    }
    public function getCustomerListWithDetail(){
        echo $this->Customer->getCustomerListWithDetail();    
    }
    public function customerSynchWithShopify(){
        echo $this->Customer->customerSynchWithShopify();    
    }
    
}

?>
