<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class CredentialController extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        $_POST  = json_decode(file_get_contents('php://input'), true);  
        $this->load->helper(array('form', 'url'));
        $this->load->model('Admin/CredentialModel', 'Credential');
	}   
   
    public function credentialStore(){
        echo $this->Credential->credentialStore(); 
    }
    public function credentialUpdate(){
        echo $this->Credential->credentialUpdate(); 
    }
    public function credentialDelete(){
        echo $this->Credential->credentialDelete(); 
    }
    public function getCredentialDataById(){
        echo $this->Credential->getCredentialDataById();    
    }
    public function getCredentialListWithDetail(){
        echo $this->Credential->getCredentialListWithDetail();    
    }
    
}

?>
