<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class OrderController extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        $_POST  = json_decode(file_get_contents('php://input'), true);  
        $this->load->helper(array('form', 'url'));
        $this->load->model('Admin/OrderModel', 'Order');
    }   

   
    public function orderStore(){
        echo $this->Order->orderStore(); 
    }
    public function orderUpdate(){
        echo $this->Order->orderUpdate(); 
    }
    public function orderDelete(){
        echo $this->Order->orderDelete(); 
    }
    public function getOrderDataById(){
        echo $this->Order->getOrderDataById();    
    }
    public function getOrderListWithDetail(){
        echo $this->Order->getOrderListWithDetail();    
    }
    public function getOrderFullfilmentDetail(){
        echo $this->Order->getOrderFullfilmentDetail();    
    }
    public function orderSynchWithShopify(){
        echo $this->Order->orderSynchWithShopify();    
    }
    
    
}

?>
