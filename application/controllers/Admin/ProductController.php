<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class ProductController extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        $_POST  = json_decode(file_get_contents('php://input'), true);  
        $this->load->helper(array('form', 'url'));
        $this->load->model('Admin/ProductModel', 'Product');
        
	}   

    
    // product section
    public function productStore(){
        echo $this->Product->productStore(); 
    }
    public function productUpdate(){
        echo $this->Product->productUpdate(); 
    }
    public function productDelete(){
        echo $this->Product->productDelete(); 
    }
    public function getProductDataById(){
        echo $this->Product->getProductDataById();    
    }
    public function getProductListWithDetail(){
        echo $this->Product->getProductListWithDetail();    
    }
    public function synchWithShopify(){
        echo $this->Product->synchWithShopify();    
    }  

}

?>
