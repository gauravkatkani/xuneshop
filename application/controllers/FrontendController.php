<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 
 * Description of Home
 *
 * @author SSV
 */
Class FrontendController extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        //$_POST  = json_decode(file_get_contents('php://input'), true);  
        $this->load->helper(array('form', 'url'));
        $this->load->model('AdminModel', 'Admin');
        
	}   

    public function index(){
        $this->load->view('frontend/layout/index');
    }

    public function submitClientDetails(){
        echo $this->Admin->submitClientDetails();
        
    }

    public function verifyClientOtp(){
        echo $this->Admin->verifyClientOtp();
        
    }

    
    

}

?>
