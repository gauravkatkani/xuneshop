<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class AdminController extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
        $_POST  = json_decode(file_get_contents('php://input'), true);  
        $this->load->helper(array('form', 'url'));
        $this->load->model('AdminModel', 'Admin');
        
	}   

    public function index(){
        $this->load->view('admin/layout/index');
    }
    public function login() {
        if(!empty($this->input->post('email')) && !empty($this->input->post('password')) ){
            echo $this->Admin->login();
        }else{
            $this->load->view('admin/login/index');
        }
    }
    public function dashboard(){
        $this->load->view('admin/dashboard/index');
    }
    public function home(){
        $this->load->view('admin/dashboard/home');
    }
    public function getCountRowsOnDashboard(){
        if(!empty($this->input->post('Admin_Auth_Token'))){
            if($this->input->post('Admin_Auth_Token') == $this->session->userdata('Admin_Auth_Token')){
                    
                $data['productCount']               = $this->Admin->getRowCount('product');
                $data['publishedProductCount']      = $this->Admin->getProductStatusCount('true'); 
                $data['unPublishedProductCount']    = $this->Admin->getProductStatusCount('false');
              
                echo json_encode(array('status'=>1,'message'=>'success','data'=>$data)); 
            }else{
                echo json_encode(array('status'=>0,'message'=>'Missing Parameter','data'=>FALSE));
            }
        }else{
            echo json_encode(array('status'=>0,'message'=>'Not Authorize','data'=>FALSE));
        }
        
    }
    
    public function getViews($module,$page){
        $this->load->view('admin/'.$module.'/'.$page);
    }
    // product section
    public function productStore(){
        echo $this->Admin->productStore(); 
    }
    public function productUpdate(){
        echo $this->Admin->productUpdate(); 
    }
    public function getProductDataById(){
        echo $this->Admin->getProductDataById();    
    }
    public function productDelete(){
        echo $this->Admin->productDelete(); 
    }
    public function getProductListWithDetail(){
        echo $this->Admin->getProductListWithDetail();    
    }
    
    //customer section
    public function client(){
        $this->load->view('admin/client/index');
    }
    public function clientShow(){
        $this->load->view('admin/client/show');
    }
    public function getClientListWithDetail(){
        echo $this->Admin->getClientListWithDetail();    
    }
    public function redeemVoucher(){
        echo $this->Admin->redeemVoucher();    
    }
    public function otpVerify(){
        echo $this->Admin->otpVerify();    
    }
    

}

?>
