-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2019 at 08:13 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xune`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `type` varchar(20) NOT NULL,
  `datetime` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `last_name`, `email`, `password`, `image`, `mobile`, `type`, `datetime`) VALUES
(1, 'admin', '', 'admin@gmail.com', 'e6e061838856bf47e1de730719fb2609', '', '', 'admin', '2019-09-24 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `values` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `body_html` varchar(100) NOT NULL,
  `vendor` varchar(100) NOT NULL,
  `product_type` varchar(100) NOT NULL,
  `tags` varchar(100) NOT NULL,
  `published` tinyint(4) NOT NULL,
  `variants` varchar(100) NOT NULL,
  `images` varchar(100) NOT NULL,
  `metafields` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `shopify` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `body_html`, `vendor`, `product_type`, `tags`, `published`, `variants`, `images`, `metafields`, `created_at`, `shopify`) VALUES
(6, 'r gr', '', 'dgdf', 'efrgg', 'grgr', 1, 'dgfdg', '', '', '2019-10-10 18:53:06', 0),
(7, 'sdfdv', '', 'xcc', 'zxxc', 'vdvdv', 1, 'vgdfgfdh', '', '', '2019-10-02 19:39:38', 1),
(9, 'sdfdv', '', 'xcc', 'zxxc', 'vdvdv', 1, 'vgdfgfdh', '', '', '2019-10-02 19:59:35', 1),
(10, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:05:07', 0),
(11, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:06:12', 0),
(12, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:11:28', 0),
(13, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:19:13', 0),
(14, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:20:50', 0),
(15, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:22:40', 0),
(16, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:23:31', 0),
(17, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:41:42', 0),
(18, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:41:56', 0),
(19, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:44:23', 0),
(20, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 19:58:17', 0),
(21, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:03:18', 0),
(22, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:04:18', 0),
(23, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:05:00', 0),
(24, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:06:05', 0),
(25, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:07:32', 0),
(26, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:08:24', 0),
(27, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:10:41', 0),
(28, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:11:23', 0),
(29, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:17:06', 0),
(30, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:18:53', 0),
(31, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:19:39', 0),
(32, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:22:38', 0),
(33, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:24:43', 0),
(34, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:25:37', 0),
(35, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:26:07', 0),
(36, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:38:11', 0),
(37, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:40:52', 0),
(38, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-11 20:53:49', 0),
(39, 'sgdgd', '', 'gfdgfd', 'gdg', 'gg', 1, 'dgdfg', '', '', '2019-10-12 19:58:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `variants`
--

CREATE TABLE `variants` (
  `id` int(11) NOT NULL,
  `option` varchar(50) NOT NULL,
  `price` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variants`
--
ALTER TABLE `variants`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `variants`
--
ALTER TABLE `variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
