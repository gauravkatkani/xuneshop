'use strict';
var AdminApp = angular.module('App', ['ui.router','toastr','ui.grid','ui.grid.selection','ui.grid.edit','ui.grid.pagination','ui.grid.pinning','ngFileUpload','ngMaterial']);
//service and factory
AdminApp.factory('storageService', ['$rootScope', function($rootScope) {
    return {
        get: function(key) {
            return localStorage.getItem(key);
        },
        set: function(key, data) {
            localStorage.setItem(key, data);
        }
    };

}]);

AdminApp.factory('AuthService', ['$rootScope', function($rootScope) {    
    var accessToken = '';
    return {
        getToken: function() {
            if(localStorage.getItem('Admin_Auth_Token')!='' && localStorage.getItem('Admin_Auth_Token')!=null && localStorage.getItem('Admin_Auth_Token')!=undefined){

                return accessToken = localStorage.getItem('Admin_Auth_Token');
            }
        },
        setToken: function(accessToken) {
            accessToken = accessToken;
        },
        isLoggedInClient : function(){
            return (localStorage.getItem('Admin_Auth_Token')!='' && localStorage.getItem('Admin_Auth_Token')!=null && localStorage.getItem('Admin_Auth_Token')!=undefined) ? true : false;
        }
    };
}]);

//config
AdminApp.config(function($stateProvider,$urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    
    $stateProvider.state({
      name: 'admin',
      url: '/',
      templateUrl: BaseUrl+'admin',
      controller: 'AdminController'
    })
    
    $stateProvider.state({
      name: 'admin.login',
      url: 'login',
      templateUrl: BaseUrl+'admin/login',
      controller: 'AdminLoginController',
    })
    .state({
      name: 'admin.dashboard',
      url: 'dashboard',     
      templateUrl: BaseUrl+'admin/dashboard',
      abstract:true,
      authenticate:true ,
      controller: 'AdminDashboardController'
    })
    .state({
      name: 'admin.dashboard.home',
      url: '/home',     
      templateUrl: BaseUrl+'admin/dashboard/home',
      controller: 'AdminDashboardHomeController'
    })
    
    //product
    .state({
      name: 'admin.dashboard.product',
      url: '/product',     
      templateUrl: BaseUrl+'admin/dashboard/product/index',
      authenticate:true
    })
    .state({
      name: 'admin.dashboard.product.show',
      url: '/show',     
      templateUrl: BaseUrl+'admin/dashboard/product/show',
      authenticate:true,  
      controller: 'DashboardProductShowController'
    })
    .state({
      name: 'admin.dashboard.product.create',
      url: '/create',     
      templateUrl: BaseUrl+'admin/dashboard/product/create',
      authenticate:true,  
      controller: 'DashboardProductCreateController'
    })
    .state({
      name: 'admin.dashboard.product.edit',
      url: '/edit/:id',     
      templateUrl: BaseUrl+'admin/dashboard/product/edit',
      authenticate:true,  
      controller: 'DashboardProductEditController'
    })
    //client
    .state({
      name: 'admin.dashboard.customer',
      url: '/customer',     
      templateUrl: BaseUrl+'admin/dashboard/customer/index',
      authenticate:true
    })
    .state({
      name: 'admin.dashboard.customer.show',
      url: '/show',     
      templateUrl: BaseUrl+'admin/dashboard/customer/show',
      authenticate:true,  
      controller: 'DashboardCustomerShowController'
    })
    .state({
      name: 'admin.dashboard.customer.create',
      url: '/create',     
      templateUrl: BaseUrl+'admin/dashboard/customer/create',
      authenticate:true,  
      controller: 'DashboardCustomerCreateController'
    })
    .state({
      name: 'admin.dashboard.customer.edit',
      url: '/edit/:id',     
      templateUrl: BaseUrl+'admin/dashboard/customer/edit',
      authenticate:true,  
      controller: 'DashboardCustomerEditController'
    })
    //order
    .state({
      name: 'admin.dashboard.order',
      url: '/order',     
      templateUrl: BaseUrl+'admin/dashboard/order/index',
      authenticate:true
    })
    .state({
      name: 'admin.dashboard.order.show',
      url: '/show',     
      templateUrl: BaseUrl+'admin/dashboard/order/show',
      authenticate:true,  
      controller: 'DashboardOrderShowController'
    })
    .state({
      name: 'admin.dashboard.order.create',
      url: '/create',     
      templateUrl: BaseUrl+'admin/dashboard/order/create',
      authenticate:true,  
      controller: 'DashboardOrderCreateController'
    })
    .state({
      name: 'admin.dashboard.order.edit',
      url: '/edit/:id',     
      templateUrl: BaseUrl+'admin/dashboard/order/edit',
      authenticate:true,  
      controller: 'DashboardOrderEditController'
    })
    .state({
      name: 'admin.dashboard.order.fullfilment',
      url: '/show',     
      templateUrl: BaseUrl+'admin/dashboard/order/fullfilment',
      authenticate:true,  
      controller: 'DashboardOrderFullfilmentController'
    })
    .state({
      name: 'admin.dashboard.credential',
      url: '/credential',     
      templateUrl: BaseUrl+'admin/dashboard/credential/index',
      authenticate:true
    })
    .state({
      name: 'admin.dashboard.credential.show',
      url: '/show',     
      templateUrl: BaseUrl+'admin/dashboard/credential/show',
      authenticate:true,  
      controller: 'DashboardCredentialShowController'
    })
    .state({
      name: 'admin.dashboard.credential.create',
      url: '/create',     
      templateUrl: BaseUrl+'admin/dashboard/credential/create',
      authenticate:true,  
      controller: 'DashboardCredentialCreateController'
    })
    .state({
      name: 'admin.dashboard.credential.edit',
      url: '/edit/:id',     
      templateUrl: BaseUrl+'admin/dashboard/credential/edit',
      authenticate:true,  
      controller: 'DashboardCredentialEditController'
    })
    

  });

    AdminApp.run(['$rootScope', '$state','$stateParams','storageService','AuthService',function($rootScope, $state,$stateParams, storageService,AuthService ) {
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        $rootScope.isLoggedInClient = AuthService.isLoggedInClient();
        // Remember toState and toStateParams.
        $rootScope.toState        = toState.name;
        $rootScope.toStateParams  = toParams;
        if (toState.authenticate && !$rootScope.isLoggedInClient) {
            
            // Abort transition
            event.preventDefault();
            // Redirect to login page
            $state.go('admin');              
        }
        if($rootScope.isLoggedInClient && ($rootScope.toState=='admin' || $rootScope.toState=='admin.login')){
          // Abort transition
          event.preventDefault();
            // Redirect to login page
            $state.go('admin.dashboard.home');     
        }             
      });

    }]);

    //controllers
    
    AdminApp.controller('AdminController', function($scope,$http,$state,toastr,storageService,$rootScope) {
        //angular.element( document.querySelector( 'body' )).removeClass('nav-md').addClass('login');
        $state.go('admin.login');
    });

    AdminApp.controller('AdminLoginController', function($scope,$http,$state,toastr,storageService,$rootScope) {
        //$rootScope.isHide   = 'true';
        angular.element( document.querySelector( 'body' )).removeClass('nav-md').addClass('login');
        $scope.email        = '';
        $scope.password     = '';
        
        $scope.submit = function(){
          $http.post(BaseUrl+'admin/login',{email:$scope.email,password:$scope.password})          
          .then(function (response){
            if(response.data.status){
              $scope.loginData = angular.copy(response.data.data);
              angular.forEach($scope.loginData,function(key,value){
                storageService.set(value,key);
              });              
              toastr.success(response.data.message);
              $state.go('admin.dashboard.home');
            }else{
              toastr.error(response.data.message);
            }
               
          });         
        }
    });
  
    AdminApp.controller('AdminDashboardController', function($scope,$http,$state,storageService) {  
        angular.element(document.body).addClass('nav-md').removeClass("login");
        $scope.activeTab = 'dashboard';
        
        //rediect of sidebar forall steps
        $scope.redirect =function(state,activeTab){
          $scope.activeTab = activeTab;
          var url = $state.href(state);
          $window.open(url,'_blank');
           //$state.go(state);
        }
        //logout for all state in admin side
        $scope.logout = function(){
          if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            $http.post(BaseUrl+'admin/logout',{Admin_Auth_Token:storageService.get('Admin_Auth_Token')})
            .then(function(response){                
              if(response.data.status){
                localStorage.clear();                    
                $state.go('admin');                   
              }
            });
          }
        } 

    });
    AdminApp.controller('AdminDashboardHomeController', function($scope,$http,$state,storageService) {  
        //angular.element(document.body).addClass('nav-md').removeClass("login");
        $scope.activeTab = 'dashboard';

        $scope.datacollection     = 0;
        $scope.emailready         = 0;
        $scope.reviewandapproved  = 0;
        $scope.waitingforprofile  = 0;
        $scope.waitingforfeedback = 0;
        $scope.interview          = 0;

        $scope.userType       = '';
        $scope.userName       = '';

        if(storageService.get('admin_full_name')!='' && storageService.get('admin_full_name')!=null && storageService.get('admin_full_name')!=undefined){
          $scope.userName = storageService.get('admin_full_name');
        }
        if(storageService.get('user_type')!='' && storageService.get('user_type')!=null && storageService.get('user_type')!=undefined){
          $scope.userType = storageService.get('userType');
        }
        

        getCountRowsOnDashboard();
        //get counts for dashboard
        function getCountRowsOnDashboard(){
          if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            $http.post(BaseUrl+'admin/getCountRowsOnDashboard',{Admin_Auth_Token:storageService.get('Admin_Auth_Token')})
            .then(function(response){                
              if(response.data.status){
             
                $scope.productCount             = response.data.data.productCount;
                $scope.publishedProductCount    = response.data.data.publishedProductCount;
                $scope.unPublishedProductCount  = response.data.data.unPublishedProductCount;
              }
            });
          }
        }

        //rediect of sidebar forall steps
        $scope.redirect =function(state,activeTab){
          $scope.activeTab = activeTab;
          var url = $state.href(state);
          $window.open(url,'_blank');
           //$state.go(state);
        }
        //logout for all state in admin side
        $scope.logout = function(){
          if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            $http.post(BaseUrl+'admin/logout',{Admin_Auth_Token:storageService.get('Admin_Auth_Token')})
            .then(function(response){                
              if(response.data.status){
                localStorage.clear();                    
                $state.go('admin');                   
              }
            });
          }
        } 

    });

    AdminApp.controller('DashboardProductShowController', ['$scope','$http','$state','storageService','$window','toastr',function($scope,$http,$state,storageService,$window,toastr) { 

        $scope.activeTab            = 'product';
        var Admin_Auth_Token        = '';
        var admin_id                = '';
        var user_type               = 'admin';

        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
          Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
          admin_id = storageService.get('admin_id');
        }

        $scope.productList = [];
        //initialize grid 
        $scope.gridOptions = {
          paginationPageSizes: [10,25,50,100,5000],
          paginationPageSize: 5000,
          enableFiltering: true,
          enableRowSelection: true,
          enableSelectAll: true,
          selectionRowHeaderWidth: 35,
          rowHeight: 35,
        
        };

        $scope.gridOptions.columnDefs = [
          { name:'Product Name', field: 'title'},
          { name:'Vendor', field: 'vendor'},      
          { name:'Product Type', field: 'product_type'},
          { name:'Tags', field: 'tags'},
          { name:'Published', field: 'published'},
          { name:'Shopify', field: 'shopify'},
          //{ name:'Variants', field: 'variants'},
          { name: 'Action', cellTemplate: '<button type="button" class="btn-small btn-primary custom-btn" ng-click="grid.appScope.editProduct(row.entity.id)" ><i class="fa fa-pencil" aria-hidden="true"></i></button><button type="button" class="btn-small btn-danger custom-btn" ng-click="grid.appScope.productDelete(row.entity.id)"><i class="fa fa-trash" aria-hidden="true"></i></button>'}
        ];

        $scope.gridOptions.data = [];

        function initialize(){
            if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!= undefined &&storageService.get('Admin_Auth_Token') !=null){
            //calling client data
                $http.post(BaseUrl+'admin/dashboard/getProductListWithDetail',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
                .then(function(response){
                    if(response.data.status){
                        $scope.productList = angular.copy(response.data.data); 
                        $scope.gridOptions.data  = $scope.productList;

                        // if($scope.productList.published == 1){
                        //     $scope.productList.published = true;    
                        // }else{
                        //     $scope.productList.published = false;  
                        // }

                        // if($scope.productList.shopify == 1){
                        //     $scope.productList.shopify = true;    
                        // }
                        // else{
                        //     $scope.productList.shopify = false;  
                        // }
                    }
                });
            }
        }

        initialize();

        $scope.gridOptions.onRegisterApi = function(gridApi){
          //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope,function(row){
                var msg = 'row selected ' + row.isSelected;
            
            }); 
            gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                var msg = 'rows changed ' + rows.length;
            
             });
        };

        $scope.editProduct = function(product_id){
            var product_id = product_id;
            if(product_id!=undefined && product_id!='' && product_id!= null){
               $state.go('admin.dashboard.product.edit',{id:product_id});         
            }
        } 

        $scope.productDelete = function(product_id){

            if(product_id!=undefined && product_id!='' && product_id!= null){
            
                $http.post(BaseUrl+'admin/dashboard/productDelete',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type'),product_id:product_id})
                .then(function(response){
                    if (response.data.status) {
                        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!= undefined &&storageService.get('Admin_Auth_Token') !=null){
                        //calling client data
                            $scope.productList = [];
                            initialize();
                        }
                        toastr.success('success',response.data.message);
                    }else{
                        toastr.error('error',response.data.message);
                    } 
                });        
            }else{
                toastr.error('error','Network Error');
            }
        }

        $scope.synchWithShopify = function(){
 
            $http.post(BaseUrl+'admin/dashboard/synchWithShopify',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
            .then(function(response){
                if (response.data.status) {
                    $scope.productList = angular.copy(response.data.data.products); 
                    $scope.gridOptions.data  = $scope.productList;

                    // if($scope.productList.published == 1){
                    //     $scope.productList.published = true;    
                    // }else{
                    //     $scope.productList.published = false;  
                    // }

                    // if($scope.productList.shopify == 1){
                    //     $scope.productList.shopify = true;    
                    // }
                    // else{
                    //     $scope.productList.shopify = false;  
                    // }
                    toastr.success('success',response.data.message);
                }else{
                    toastr.error('error',response.data.message);
                } 
            });        
        }

        $scope.submit = function(){

            $http.post(BaseUrl+'admin/dashboard/createProduct',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
            .then(function(response){
                if(response.data.status){
                    $scope.companygridoptions = angular.copy(response.data.data); 
                        
                }
            });
        }
    }]);

    AdminApp.controller('DashboardProductCreateController', ['$scope','$http','storageService','$window','$filter','$state','toastr','Upload','$element',function($scope,$http,storageService,$window,$filter,$state,toastr,Upload,$element) { 
        var Admin_Auth_Token      = '';
        var admin_id              = '';
        var user_type             = 'admin';    
        $scope.product_name       = '';
        $scope.product_type       = '';
        $scope.vendor             = '';
        $scope.tags               = '';
        $scope.publish            = '';
        $scope.variant            = '';
        $scope.shopify            = '';
        $scope.price              = '';
        $scope.file               = [];
      
        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            var Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
            var admin_id = storageService.get('admin_id');
        }

        $scope.submit = function(){

            if($scope.publish == true){
                $scope.publish = 1;
            }else{
                $scope.publish = 0;
            }
            
            if($scope.shopify == true){
                $scope.shopify = 1;
            }else{
                $scope.shopify = 0;
            }
            
            var data = {
              Admin_Auth_Token    : Admin_Auth_Token,
              user_type           : user_type,
              product_name        : $scope.product_name ,
              product_type        : $scope.product_type,
              vendor              : $scope.vendor,
              tags                : $scope.tags,
              publish             : $scope.publish,
              shopify             : $scope.shopify,
              price               : $scope.price,
              variant             : $scope.variant 
            };
            
            Upload.upload({
              url: BaseUrl+'admin/dashboard/productStore',
              method: 'POST',
              file:$scope.file,
              data:data
              
            }).then(function (response) {
              if (response.data.status) {
                toastr.success('success',response.data.message);
                $state.go('admin.dashboard.product.show'); 
              }else{
                toastr.error('error',response.data.message);
              }          
            });
        }

    }]);

    AdminApp.controller('DashboardProductEditController', ['$scope','$http','$stateParams','storageService','$window','$filter','$state','toastr','Upload', function($scope,$http,$stateParams,storageService,$window,$filter,$state,toastr,Upload) { 
        var Admin_Auth_Token      = '';
        var admin_id              = '';
        var user_type             = 'admin';    
        $scope.product_name       = '';
        $scope.product_type       = '';
        $scope.vendor             = '';
        $scope.tags               = '';
        $scope.publish            = '';
        $scope.variant            = '';
        $scope.shopify            = '';
        $scope.price              = '';
        $scope.file               = [];
        $scope.productDetail      = [];
        $scope.product_id          = $stateParams.id;

        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
          var Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
          var admin_id = storageService.get('admin_id');
        }
        
        $http.post(BaseUrl+'admin/dashboard/getProductDataById',{Admin_Auth_Token:Admin_Auth_Token,user_type:user_type,product_id:$scope.product_id})
        .then(function(response){
            if(response.data.status)
            {        
                $scope.productDetail        = response.data.data;
           
                $scope.product_name         = $scope.productDetail.title;
                $scope.product_type         = $scope.productDetail.product_type;
                $scope.vendor               = $scope.productDetail.vendor;
                $scope.tags                 = $scope.productDetail.tags;
                $scope.price                = $scope.productDetail.price;
                // $scope.shopify              = $scope.productDetail.shopify;
                $scope.old_img              = $scope.productDetail.images;  
                $scope.variant              = $scope.productDetail.variants; 

                if($scope.productDetail.published == 1){
                    $scope.publish = true;    
                }else{
                    $scope.publish = false;  
                }

                if($scope.productDetail.shopify == 1){
                    $scope.shopify = true;    
                }
                else{
                    $scope.shopify = false;  
                }

            }     
              
            $scope.submit = function(){

                if($scope.publish == true){
                    $scope.publish = 1;
                }else{
                     $scope.publish = 0;
                }

                if($scope.shopify == true){
                    $scope.shopify = 1;
                }else{
                     $scope.shopify = 0;
                }
      
                var data = {
                  Admin_Auth_Token    : Admin_Auth_Token,
                  user_type           : user_type,
                  product_id          : $scope.product_id,
                  product_name        : $scope.product_name ,
                  product_type        : $scope.product_type,
                  vendor              : $scope.vendor,
                  tags                : $scope.tags,
                  publish             : $scope.publish,
                  shopify             : $scope.shopify, 
                  price               : $scope.price,
                  variant             : $scope.variant 
                };
        
                Upload.upload({
                    url: BaseUrl+'admin/dashboard/productUpdate',
                    method: 'POST',
                    file:$scope.file,
                    data:data
                    
                }).then(function (response) {
                    if (response.data.status) {
                      toastr.success('success',response.data.message);
                      $state.go('admin.dashboard.product.show'); 
                    }else{
                      toastr.error('error',response.data.message);
                    }          
                });
            }  
            
        });
    }]);

    AdminApp.controller('DashboardCustomerShowController', ['$scope','$http','$state','storageService','$window','toastr','$mdDialog',function($scope,$http,$state,storageService,$window,toastr,$mdDialog) { 

        $scope.activeTab            = 'customer';
        var Admin_Auth_Token        = '';
        var admin_id                = '';
        $scope.otp                  = '' ;
        $scope.invoice              = '';
        var user_type               = 'admin';

        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
            admin_id = storageService.get('admin_id');
        }

        $scope.customerList = [];
        //initialize grid 
        $scope.gridOptions = {
            paginationPageSizes: [10,25,50,100,5000],
            paginationPageSize: 5000,
            enableFiltering: true,
            enableRowSelection: true,
            enableSelectAll: true,
            selectionRowHeaderWidth: 35,
            rowHeight: 35,
        
        };

        $scope.gridOptions.columnDefs = [
            { name:'First name', width:100,enableCellEdit: false, field: 'first_name'},
            { name:'Last name', width:100,enableCellEdit: false, field: 'last_name'},      
            { name:'Mobile No.',width:150,enableCellEdit: false, field: 'phone'},
            { name:'Email',width:150,enableCellEdit: false, field: 'email'},
            { name:'Address', width:150,enableCellEdit: false,field: 'address'},
            { name:'City', width:100,enableCellEdit: false, field: 'city'},
            { name:'Country',width:100,enableCellEdit: false, field: 'country'},
            //{ name:'Status', field: 'voucher_status', cellTemplate: '<span style="color:green" ng-hide="row.entity.voucher_status=='' || row.entity.voucher_status==null || row.entity.voucher_status==undefined">UnVerify</span><span style="color:red" ng-show="(row.entity.voucher_status!='' || row.entity.voucher_status!=null || row.entity.voucher_status!=undefined">UnVerify</span>'},
            { name: 'Action',width:100, cellTemplate: '<button type="button" class="btn-small btn-primary custom-btn" ng-click="grid.appScope.editCustomer(row.entity.id)" ><i class="fa fa-pencil" aria-hidden="true"></i></button><button type="button" class="btn-small btn-danger custom-btn" ng-click="grid.appScope.customerDelete(row.entity.id)"><i class="fa fa-trash" aria-hidden="true"></i></button>'}
        ];

        $scope.gridOptions.data = [];

        function initialize(){
            if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!= undefined &&storageService.get('Admin_Auth_Token') !=null){
                //calling client data
                $http.post(BaseUrl+'admin/dashboard/getCustomerListWithDetail',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
                .then(function(response){
                    if(response.data.status){
                        $scope.clientList = angular.copy(response.data.data); 
                        // angular.forEach($scope.clientList,function(item){                    
                        //     item.interest = item.interest.replace(/[\[\]']/g,'' );                    
                        // });
                        $scope.gridOptions.data  = $scope.clientList;
                    }
                });
            }
        }

        initialize();

        $scope.gridOptions.onRegisterApi = function(gridApi){
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope,function(row){
                var msg = 'row selected ' + row.isSelected;
            
            }); 
            gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                var msg = 'rows changed ' + rows.length;
            
            });
        };

        $scope.editCustomer = function(customer_id){
            var customer_id = customer_id;
            if(customer_id!=undefined && customer_id!='' && customer_id!= null){
                $state.go('admin.dashboard.customer.edit',{id:customer_id});         
            }
        } 

        $scope.customerDelete = function(customer_id){

            if(customer_id!=undefined && customer_id!='' && customer_id!= null){
            
                $http.post(BaseUrl+'admin/dashboard/customerDelete',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type'),customer_id:customer_id})
                .then(function(response){
                    if (response.data.status) {
                        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!= undefined &&storageService.get('Admin_Auth_Token') !=null){
                            //calling client data
                            $scope.customerList = [];
                            initialize();
                        }
                        toastr.success('success',response.data.message);
                    }else{
                        toastr.error('error',response.data.message);
                    } 
                });        

            }else{
                toastr.error('error','Network Error');
            }
        }

        $scope.customerSynchWithShopify = function(){
 
            $http.post(BaseUrl+'admin/dashboard/customerSynchWithShopify',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
            .then(function(response){
                if (response.data.status) {
                    // $scope.clientList = angular.copy(response.data.data.customers); 
                    // $scope.gridOptions.data  = $scope.clientList;
                    initialize();
                    toastr.success('success',response.data.message);
                }else{
                    toastr.error('error',response.data.message);
                } 
            });        
        }

        $scope.submit = function(){

            $http.post(BaseUrl+'admin/dashboard/createCustomer',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
            .then(function(response){
                if(response.data.status){
                $scope.customergridoptions = angular.copy(response.data.data); 
                        
                }
            });
        }

    }]);

    AdminApp.controller('DashboardCustomerCreateController', ['$scope','$http','storageService','$window','$filter','$state','toastr','Upload','$element',function($scope,$http,storageService,$window,$filter,$state,toastr,Upload,$element) { 
        var Admin_Auth_Token        = '';
        var admin_id                = '';
        var user_type               = 'admin';    
        $scope.first_name           = '';
        $scope.last_name            = '';
        $scope.email                = '';
        $scope.phone                = '';
        $scope.send_email_invite    = '';
        $scope.city                 = '';
        $scope.address              = '';
        $scope.country              = '';
      
        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            var Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
            var admin_id = storageService.get('admin_id');
        }

        $scope.submit = function(){
            
            var data = {
                Admin_Auth_Token    : Admin_Auth_Token,
                user_type           : user_type,
                first_name          : $scope.first_name ,
                last_name           : $scope.last_name,
                email               : $scope.email,
                phone               : $scope.phone,
                send_email_invite   : $scope.send_email_invite,
                address             : $scope.address,
                city                : $scope.city,
                country             : $scope.country 
            };
            
            $http.post(BaseUrl+'admin/dashboard/customerStore',data)
            .then(function(response){
              if (response.data.status) {
                toastr.success('success',response.data.message);
                $state.go('admin.dashboard.customer.show'); 
              }else{
                toastr.error('error',response.data.message);
              }          
            });
        }

    }]);

    AdminApp.controller('DashboardCustomerEditController', ['$scope','$http','$stateParams','storageService','$window','$filter','$state','toastr','Upload', function($scope,$http,$stateParams,storageService,$window,$filter,$state,toastr,Upload) { 
        var Admin_Auth_Token      = '';
        var admin_id              = '';
        var user_type             = 'admin';    
        $scope.first_name           = '';
        $scope.last_name            = '';
        $scope.email                = '';
        $scope.phone                = '';
        $scope.send_email_invite    = '';
        $scope.city                 = '';
        $scope.address              = '';
        $scope.country              = '';
        $scope.customerDetail        = [];
        $scope.customer_id           = $stateParams.id;

        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
          var Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
          var admin_id = storageService.get('admin_id');
        }
        
        $http.post(BaseUrl+'admin/dashboard/getCustomerDataById',{Admin_Auth_Token:Admin_Auth_Token,user_type:user_type,customer_id:$scope.customer_id})
        .then(function(response){
            if(response.data.status)
            {        
                $scope.customerDetail       = response.data.data;

                console.log($scope.customerDetail);
           
                $scope.first_name           = $scope.customerDetail.first_name;
                $scope.last_name            = $scope.customerDetail.last_name;
                $scope.email                = $scope.customerDetail.email;
                $scope.phone                = $scope.customerDetail.phone;
                $scope.send_email_invite    = $scope.customerDetail.send_email_invite;
                $scope.country              = $scope.customerDetail.country;
                $scope.city                 = $scope.customerDetail.city;  
                $scope.address              = $scope.customerDetail.address; 

                // if($scope.customerDetail.shopify == 1){
                //     $scope.shopify = true;    
                // }
                // else{
                //     $scope.shopify = false;  
                // }

            }     
              
            $scope.submit = function(){
      
                var data = {
                    Admin_Auth_Token    : Admin_Auth_Token,
                    user_type           : user_type,
                    customer_id         : $scope.customer_id ,
                    first_name          : $scope.first_name ,
                    last_name           : $scope.last_name,
                    email               : $scope.email,
                    phone               : $scope.phone,
                    send_email_invite   : $scope.send_email_invite,
                    address             : $scope.address,
                    city                : $scope.city,
                    country             : $scope.country 
                };

                $http.post(BaseUrl+'admin/dashboard/customerUpdate',data)
                .then(function (response) {
                    if (response.data.status) {
                      toastr.success('success',response.data.message);
                      $state.go('admin.dashboard.customer.show'); 
                    }else{
                      toastr.error('error',response.data.message);
                    }          
                });
            }  
            
        });
    }]);

    AdminApp.controller('DashboardOrderShowController', ['$scope','$http','$state','storageService','$window','toastr','$mdDialog',function($scope,$http,$state,storageService,$window,toastr,$mdDialog) { 

        $scope.activeTab            = 'order';
        var Admin_Auth_Token        = '';
        var admin_id                = '';
        var user_type               = 'admin';

        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
            admin_id = storageService.get('admin_id');
        }

        $scope.orderList = [];
        //initialize grid 
        $scope.gridOptions = {
            paginationPageSizes: [10,25,50,100,5000],
            paginationPageSize: 5000,
            enableFiltering: true,
            enableRowSelection: true,
            enableSelectAll: true,
            selectionRowHeaderWidth: 35,
            rowHeight: 35,
        
        };

        $scope.gridOptions.columnDefs = [
            { name:'Order No.',width:100,enableCellEdit: false, field: 'order_number'},
            { name:'Name',width:150,enableCellEdit: false, field: 'billing_address.name'},
            { name:'Email', width:200,enableCellEdit: false, field: 'email'},
            { name:'Status', width:100,enableCellEdit: false, field: 'financial_status'},      
            { name:'Price',width:80,enableCellEdit: false, field: 'total_price'},
            { name:'City',width:80,enableCellEdit: false, field: 'billing_address.city'},
            { name:'State',width:100,enableCellEdit: false, field: 'billing_address.province'},
            { name:'Country',width:100,enableCellEdit: false, field: 'billing_address.country'},
            { name:'Payment', widPth:250,enableCellEdit: false,field: 'gateway'},
            //{ name: 'Action',width:100, cellTemplate: '<button type="button" class="btn-small btn-primary custom-btn" ng-click="grid.appScope.getFullfilment(row.entity.customer.last_order_id)">Fullfilment</button>'}
        ];

        $scope.gridOptions.data = [];

        function initialize(){
            if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!= undefined &&storageService.get('Admin_Auth_Token') !=null){
                //calling client data
                $http.post(BaseUrl+'admin/dashboard/getOrderListWithDetail',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
                .then(function(response){
                    if(response.data.status){

                      $scope.orderList = angular.copy(response.data.data.orders);
                      $scope.gridOptions.data  = $scope.orderList; 

                    }
                });
            }
        }

        initialize();

        $scope.gridOptions.onRegisterApi = function(gridApi){
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope,function(row){
                var msg = 'row selected ' + row.isSelected;
            
            }); 
            gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                var msg = 'rows changed ' + rows.length;
            
            });
        };

        $scope.getFullfilment = function(order_id){
            var order_id = order_id;
            if(order_id!=undefined && order_id!='' && order_id!= null){
                $http.post(BaseUrl+'admin/dashboard/getOrderFullfilmentDetail',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type'),order_id:order_id})
                .then(function(response){
                    if (response.data.status) {
                        toastr.success('success',response.data.message);
                        $state.go('admin.dashboard.customer.fullfilment'); 
                    }else{
                        toastr.error('error',response.data.message);
                    } 
                });      
            }
        } 

        $scope.orderDelete = function(order_id){

            if(order_id!=undefined && order_id!='' && order_id!= null){
            
                $http.post(BaseUrl+'admin/dashboard/orderDelete',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type'),order_id:order_id})
                .then(function(response){
                    if (response.data.status) {
                        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!= undefined &&storageService.get('Admin_Auth_Token') !=null){
                            //calling client data
                            $scope.orderList = [];
                            initialize();
                        }
                        toastr.success('success',response.data.message);
                    }else{
                        toastr.error('error',response.data.message);
                    } 
                });        

            }else{
                toastr.error('error','Network Error');
            }
        }

        $scope.orderSynchWithShopify = function(){
 
            $http.post(BaseUrl+'admin/dashboard/orderSynchWithShopify',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
            .then(function(response){
                if (response.data.status) {
                    $scope.orderList = angular.copy(response.data.data.orders); 
                    $scope.gridOptions.data  = $scope.orderList;
                    toastr.success('success',response.data.message);
                }else{
                    toastr.error('error',response.data.message);
                } 
            });        
        }

        $scope.submit = function(){

            $http.post(BaseUrl+'admin/dashboard/createOrder',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
            .then(function(response){
                if(response.data.status){
                $scope.ordergridoptions = angular.copy(response.data.data); 
                        
                }
            });
        }

    }]);


    AdminApp.controller('DashboardOrderFullfilmentController', ['$scope','$http','$state','storageService','$window','toastr','$mdDialog',function($scope,$http,$state,storageService,$window,toastr,$mdDialog) { 

        $scope.activeTab            = 'order';
        var Admin_Auth_Token        = '';
        var admin_id                = '';
        var user_type               = 'admin';

        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
            admin_id = storageService.get('admin_id');
        }

        $scope.orderList = [];
        //initialize grid 
        $scope.gridOptions = {
            paginationPageSizes: [10,25,50,100,5000],
            paginationPageSize: 5000,
            enableFiltering: true,
            enableRowSelection: true,
            enableSelectAll: true,
            selectionRowHeaderWidth: 35,
            rowHeight: 35,
        
        };

        $scope.gridOptions.columnDefs = [
            { name:'Order No.',width:100,enableCellEdit: false, field: 'order_number'},
            { name:'Name',width:150,enableCellEdit: false, field: 'billing_address.name'},
            { name:'Email', width:200,enableCellEdit: false, field: 'email'},
            { name:'Status', width:100,enableCellEdit: false, field: 'financial_status'},      
            { name:'Price',width:80,enableCellEdit: false, field: 'total_price'},
            { name:'City',width:80,enableCellEdit: false, field: 'billing_address.city'},
            { name:'State',width:100,enableCellEdit: false, field: 'billing_address.province'},
            { name:'Country',width:100,enableCellEdit: false, field: 'billing_address.country'},
            { name:'Payment', width:150,enableCellEdit: false,field: 'gateway'},
            { name: 'Action', cellTemplate: '<button type="button" class="btn-small btn-primary custom-btn" ng-click="grid.appScope.getFullfilment(row.entity.customer.last_order_id)">Fullfilment</button>'}
        ];

        $scope.gridOptions.data = [];

        function initialize(){
            if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!= undefined &&storageService.get('Admin_Auth_Token') !=null){
                //calling client data
                $http.post(BaseUrl+'admin/dashboard/getOrderListWithDetail',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
                .then(function(response){
                    if(response.data.status){

                      $scope.orderList = angular.copy(response.data.data.orders);
                      $scope.gridOptions.data  = $scope.orderList; 

                    }
                });
            }
        }

        initialize();

        $scope.gridOptions.onRegisterApi = function(gridApi){
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope,function(row){
                var msg = 'row selected ' + row.isSelected;
            
            }); 
            gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                var msg = 'rows changed ' + rows.length;
            
            });
        };

        $scope.getFullfilment = function(order_id){
            var order_id = order_id;
            if(order_id!=undefined && order_id!='' && order_id!= null){
                $http.post(BaseUrl+'admin/dashboard/getOrderFullfilmentDetail',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type'),order_id:order_id})
                .then(function(response){
                    if (response.data.status) {
                        toastr.success('success',response.data.message);
                        $state.go('admin.dashboard.customer.fullfilment'); 
                    }else{
                        toastr.error('error',response.data.message);
                    } 
                });      
            }
        } 

    }]);
    
    AdminApp.controller('DashboardCredentialShowController', ['$scope','$http','$state','storageService','$window','toastr','AuthService',function($scope,$http,$state,storageService,$window,toastr,AuthService) { 

        $scope.activeTab            = 'credential';
        var Admin_Auth_Token        = '';
        var admin_id                = '';
        var user_type               = 'admin';

        // Admin_Auth_Token = AuthService.getToken();

        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
            admin_id = storageService.get('admin_id');
        }

        $scope.keyList = [];
        //initialize grid 
        $scope.gridOptions = {
            paginationPageSizes: [10,25,50,100,5000],
            paginationPageSize: 5000,
            enableFiltering: true,
            enableRowSelection: true,
            enableSelectAll: true,
            selectionRowHeaderWidth: 35,
            rowHeight: 35,
        
        };

        $scope.gridOptions.columnDefs = [
            { name:'Vendor', field: 'vendor'},
            { name:'ApiKey', field: 'api_key'},      
            { name:'Secret', field: 'api_secret'},
            { name:'Password', field: 'password'},
            { name:'Url',field:'shop_url'},
            { name:'Action', cellTemplate: '<button type="button" class="btn-small btn-primary custom-btn" ng-click="grid.appScope.editCredential(row.entity.id)" ><i class="fa fa-pencil" aria-hidden="true"></i></button><button type="button" class="btn-small btn-danger custom-btn" ng-click="grid.appScope.credentialDelete(row.entity.id)"><i class="fa fa-trash" aria-hidden="true"></i></button>'}
        ];

        $scope.gridOptions.data = [];

        function initialize(){
            if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!= undefined &&storageService.get('Admin_Auth_Token') !=null){
            //calling client data
                $http.post(BaseUrl+'admin/dashboard/getCredentialListWithDetail',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type')})
                .then(function(response){
                    if(response.data.status){
                        $scope.credentialList   = angular.copy(response.data.data); 
                        $scope.gridOptions.data = $scope.credentialList;
                    }
                });
            }
        }

        initialize();

        $scope.gridOptions.onRegisterApi = function(gridApi){
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope,function(row){
                var msg = 'row selected ' + row.isSelected;
              
            }); 
            gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                var msg = 'rows changed ' + rows.length;
              
            });
        };

        $scope.editCredential = function(credential_id){
            var credential_id = credential_id;
            if(credential_id!=undefined && credential_id!='' && credential_id!= null){
                $state.go('admin.dashboard.credential.edit',{id:credential_id});         
            }
        } 

        $scope.credentialDelete = function(credential_id){

            if(credential_id!=undefined && credential_id!='' && credential_id!= null){
              
                $http.post(BaseUrl+'admin/dashboard/credentialDelete',{Admin_Auth_Token:storageService.get('Admin_Auth_Token'),user_type:storageService.get('user_type'),credential_id:credential_id})
                .then(function(response){
                    if (response.data.status) {
                        $scope.credentialList = [];
                        initialize();
                        toastr.success('success',response.data.message);
                    }else{
                        toastr.error('error',response.data.message);
                    } 
                });        

            }else{
                toastr.error('error','Network Error');
            }
        }
    }]);

    AdminApp.controller('DashboardCredentialCreateController', ['$scope','$http','storageService','$window','$filter','$state','toastr','Upload','$element',function($scope,$http,storageService,$window,$filter,$state,toastr,Upload,$element) { 
        var Admin_Auth_Token      = '';
        var admin_id              = '';
        var user_type             = 'admin';    
        $scope.vendor             = 'select';
        $scope.apikey             = '';
        $scope.secret             = '';
        $scope.password           = '';
        $scope.url                = '';
        $scope.vendorList         = [{id:'select',value:'Please Select'},{id:'shopify',value:'Shopify Store'}];
      
        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
            var Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
            var admin_id = storageService.get('admin_id');
        }

        $scope.submit = function(){
            
            var data = {
                Admin_Auth_Token      : Admin_Auth_Token,
                user_type             : user_type,
                vendor                : $scope.vendor ,
                apikey                : $scope.apikey,
                secret                : $scope.secret,
                password              : $scope.password,
                url                   : $scope.url 
            };

            $http.post(BaseUrl+'admin/dashboard/credentialStore',data)
            .then(function (response) {
              if (response.data.status) {
                toastr.success('success',response.data.message);
                $state.go('admin.dashboard.credential.show'); 
              }else{
                toastr.error('error',response.data.message);
              }          
            });
        }

    }]);

    AdminApp.controller('DashboardCredentialEditController', ['$scope','$http','$stateParams','storageService','$window','$filter','$state','toastr','Upload', function($scope,$http,$stateParams,storageService,$window,$filter,$state,toastr,Upload) { 
        var Admin_Auth_Token      = '';
        var admin_id              = '';
        var user_type             = 'admin';    
        $scope.vendor             = '';
        $scope.apikey             = '';
        $scope.secret             = '';
        $scope.password           = '';
        $scope.url                = '';
        $scope.credentialDetail   = [];
        $scope.credential_id      = $stateParams.id;
        $scope.vendorList         = [{id:'select',value:'Please Select'},{id:'shopify',value:'Shopify Store'}];

        if(storageService.get('Admin_Auth_Token')!='' && storageService.get('Admin_Auth_Token')!=null && storageService.get('Admin_Auth_Token')!=undefined){
          var Admin_Auth_Token = storageService.get('Admin_Auth_Token');
        }
        if(storageService.get('admin_id')!='' && storageService.get('admin_id')!=null && storageService.get('admin_id')!=undefined){
          var admin_id = storageService.get('admin_id');
        }
        
        $http.post(BaseUrl+'admin/dashboard/getCredentialDataById',{Admin_Auth_Token:Admin_Auth_Token,user_type:user_type,credential_id:$scope.credential_id})
        .then(function(response){
            if(response.data.status)
            {        
                $scope.credentialDetail     = response.data.data;
                $scope.vendor               = $scope.credentialDetail.vendor;
                $scope.apikey               = $scope.credentialDetail.api_key;
                $scope.secret               = $scope.credentialDetail.api_secret;
                $scope.password             = $scope.credentialDetail.password;
                $scope.url                  = $scope.credentialDetail.shop_url;
            }     
              
            $scope.submit = function(){
      
                var data = {
                    Admin_Auth_Token        : Admin_Auth_Token,
                    user_type               : user_type,
                    credential_id           : $scope.credential_id ,
                    vendor                  : $scope.vendor ,
                    apikey                  : $scope.apikey,
                    secret                  : $scope.secret,
                    password                : $scope.password,
                    url                     : $scope.url 
                };

                $http.post(BaseUrl+'admin/dashboard/credentialUpdate',data)
                .then(function (response) {
                    if (response.data.status) {
                      toastr.success('success',response.data.message);
                      $state.go('admin.dashboard.credential.show'); 
                    }else{
                      toastr.error('error',response.data.message);
                    }          
                });
            }  
            
        });
    }]);